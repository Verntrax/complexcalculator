# Complex calculator
Complex calculator introduces console application for performing simple,
double precision computations on complex numbers including addition,
subtraction, multiplication, division, conjugation and power. Round brackets 
can be used as well. Results of expressions can be assigned to a variable 
and used in next computations.

## Starting application
If you want to start the application, you need to have Java 8 (or higher)
installed. If Java is added to your PATH, simply run the following command
from shell in the JAR file location:
```shell
$ java -jar complexCalculator-1.1.jar
```
```complexCalculator
>> 1.5i * (12 + 3i)
-4.5+18i
```

## Examples
* Perform calculations:
```complexCalculator
>> 3i*5 + 6/2i*
18i
```
* Assign results to variables and use them:
```complexCalculator
>> var = -(2i + 5)*/2
-2.5+1i
>> x = -0.1^2
-0.01
>> y = var*x - 1i
0.025-1.01i
```
* Print variables:
```complexCalculator
>> print x var
x = -0.01
var = -2.5+1i
>> print
var = -2.5+1i
x = -0.01
y = 0.025-1.01i
```
* Clear variables:
```complexCalculator
>> clear var
>> print
x = -0.01
y = 0.025-1.01i
>> clear
>> print
>> 
```
* Exit program:
```complexCalculator
>> exit
$
```

## Downloads
All releases can be downloaded [here](https://gitlab.com/Verntrax/complexcalculator/tags)

## License
[MIT License](LICENSE)