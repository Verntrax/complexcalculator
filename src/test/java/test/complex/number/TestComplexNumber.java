package test.complex.number;

import complex.number.ComplexNumber;
import complex.number.exception.ComplexNumberException;
import org.junit.Assert;

public class TestComplexNumber {
    @org.junit.Test
    public void testToStringZero() {
        ComplexNumber number = new ComplexNumber(0, 0);
        String expected = "0";

        Assert.assertEquals(expected, number.toString());
    }

    @org.junit.Test
    public void testToStringZeroIm() {
        ComplexNumber number = new ComplexNumber(5.2, 0);
        String expected = "5.2";

        Assert.assertEquals(expected, number.toString());
    }

    @org.junit.Test
    public void testToStringZeroRe() {
        ComplexNumber number = new ComplexNumber(0, -0.048);
        String expected = "-0.048i";

        Assert.assertEquals(expected, number.toString());
    }

    @org.junit.Test
    public void testToStringPositiveIm() {
        ComplexNumber number = new ComplexNumber(-2.48, 3);
        String expected = "-2.48+3i";

        Assert.assertEquals(expected, number.toString());
    }

    @org.junit.Test
    public void testToStringNegativeIm() {
        ComplexNumber number = new ComplexNumber(15.33, -12.567);
        String expected = "15.33-12.567i";

        Assert.assertEquals(expected, number.toString());
    }

    @org.junit.Test
    public void testEqualsEqual() {
        ComplexNumber first = new ComplexNumber(5.4, 2.8);
        ComplexNumber second = new ComplexNumber(5.4, 2.8);

        Assert.assertEquals(first, second);
    }

    @org.junit.Test
    public void testEqualsNotEqual() {
        ComplexNumber first = new ComplexNumber(-0.5, 7.3);
        ComplexNumber second = new ComplexNumber(-0.5, 2.2);

        Assert.assertNotEquals(first, second);
    }

    @org.junit.Test
    public void testEqualsOtherObject() {
        ComplexNumber first = new ComplexNumber(2.1, -3.55);
        String second = "2.1-3.55i";

        Assert.assertNotEquals(first, second);
    }

    @org.junit.Test
    public void testCopyConstructor() {
        ComplexNumber parent = new ComplexNumber(2.88, -3.4);
        ComplexNumber child = new ComplexNumber(parent);

        Assert.assertEquals(parent, child);
    }

    @org.junit.Test
    public void testNegation() {
        ComplexNumber number = new ComplexNumber(2.5, 3);
        ComplexNumber expected = new ComplexNumber(-2.5, -3);

        Assert.assertEquals(expected, number.negate());
    }

    @org.junit.Test
    public void testConjugation() {
        ComplexNumber number = new ComplexNumber(2.5, -4.2);
        ComplexNumber expected = new ComplexNumber(2.5, 4.2);

        Assert.assertEquals(expected, number.conjugate());
    }

    @org.junit.Test
    public void testAddition() {
        ComplexNumber first = new ComplexNumber(23, -18);
        ComplexNumber second = new ComplexNumber(-2, -3.5);

        ComplexNumber result = first.add(second);
        ComplexNumber expected = new ComplexNumber(21, -21.5);

        Assert.assertEquals(expected, result);
    }

    @org.junit.Test(expected = ComplexNumberException.class)
    public void testAdditionOverflow() {
        ComplexNumber first = new ComplexNumber(Double.MAX_VALUE, 0);
        ComplexNumber second = new ComplexNumber(Double.MAX_VALUE, 2);
        try {
            first.add(second);
        } catch (ComplexNumberException e) {
            Assert.assertTrue(e.getMessage().contains("addition"));
            throw e;
        }
    }

    @org.junit.Test
    public void testSubtraction() {
        ComplexNumber first = new ComplexNumber(4.55, 2.3);
        ComplexNumber second = new ComplexNumber(-1, 2);

        ComplexNumber result = first.subtract(second);
        ComplexNumber expected = new ComplexNumber(5.55, 0.3);

        Assert.assertEquals(expected, result);
    }

    @org.junit.Test(expected = ComplexNumberException.class)
    public void testSubtractionOverflow() {
        ComplexNumber first = new ComplexNumber(0, -Double.MAX_VALUE);
        ComplexNumber second = new ComplexNumber(3, Double.MAX_VALUE);
        try {
            first.subtract(second);
        } catch (ComplexNumberException e) {
            Assert.assertTrue(e.getMessage().contains("subtraction"));
            throw e;
        }
    }

    @org.junit.Test
    public void testMultiplication() {
        ComplexNumber first = new ComplexNumber(1, 2);
        ComplexNumber second = new ComplexNumber(-2, 3);

        ComplexNumber result = first.multiply(second);
        ComplexNumber expected = new ComplexNumber(-8, -1);

        Assert.assertEquals(expected, result);
    }

    @org.junit.Test(expected = ComplexNumberException.class)
    public void testMultiplicationOverflow() {
        ComplexNumber first = new ComplexNumber(Double.MAX_VALUE, 5);
        ComplexNumber second = new ComplexNumber(0, Double.MAX_VALUE);
        try {
            first.multiply(second);
        } catch (ComplexNumberException e) {
            Assert.assertTrue(e.getMessage().contains("multiplication"));
            throw e;
        }
    }

    @org.junit.Test
    public void testDivision() {
        ComplexNumber first = new ComplexNumber(-2, 4);
        ComplexNumber second = new ComplexNumber(3, -1);

        ComplexNumber result = first.divide(second);
        ComplexNumber expected = new ComplexNumber(-1, 1);

        Assert.assertEquals(expected, result);
    }

    @org.junit.Test(expected = ComplexNumberException.class)
    public void testUndefinedDivision() {
        ComplexNumber first = new ComplexNumber(2, 1.8);
        ComplexNumber second = new ComplexNumber(0,0);
        try {
            first.divide(second);
        } catch (ComplexNumberException e) {
            Assert.assertTrue(e.getMessage().contains("division"));
            throw e;
        }
    }

    @org.junit.Test(expected = ComplexNumberException.class)
    public void testDivisionOverflow() {
        ComplexNumber first = new ComplexNumber(2, -3.56);
        ComplexNumber second = new ComplexNumber(Double.MIN_VALUE,0);
        try {
            first.divide(second);
        } catch (ComplexNumberException e) {
            Assert.assertTrue(e.getMessage().contains("division"));
            throw e;
        }
    }

    @org.junit.Test
    public void testIntToIntPower() {
        ComplexNumber first = new ComplexNumber(2, 0);
        ComplexNumber second = new ComplexNumber(3, 0);

        ComplexNumber result = first.power(second);
        ComplexNumber expected = new ComplexNumber(8, 0);

        Assert.assertEquals(expected, result);
    }

    @org.junit.Test
    public void testDoubleToDoublePower() {
        ComplexNumber first = new ComplexNumber(2.35, 0);
        ComplexNumber second = new ComplexNumber(-0.78, 0);

        ComplexNumber result = first.power(second);
        ComplexNumber expected = new ComplexNumber(Math.pow(2.35, -0.78), 0);

        Assert.assertEquals(expected, result);
    }

    @org.junit.Test
    public void testDoubleToImaginaryPower() {
        ComplexNumber first = new ComplexNumber(Math.E, 0);
        ComplexNumber second = new ComplexNumber(0, Math.PI);

        ComplexNumber result = first.power(second);
        ComplexNumber expected = new ComplexNumber(-1, 0);

        Assert.assertEquals(expected, result);
    }

    @org.junit.Test
    public void testImaginaryToDoublePower() {
        ComplexNumber first = new ComplexNumber(0, 1);
        ComplexNumber second = new ComplexNumber(0.5, 0);

        ComplexNumber result = first.power(second);
        ComplexNumber expected = new ComplexNumber(Math.sqrt(2)/2, Math.sqrt(2)/2);

        Assert.assertEquals(expected, result);
    }

    @org.junit.Test
    public void testComplexToDoublePower() {
        ComplexNumber first = new ComplexNumber(-0.5, 2);
        ComplexNumber second = new ComplexNumber(0.33, 0);

        ComplexNumber result = first.power(second);
        ComplexNumber expected = new ComplexNumber(1.0484568094620832, 0.7160660602472992);

        Assert.assertEquals(expected, result);
    }

    @org.junit.Test
    public void testDoubleToComplexPower() {
        ComplexNumber first = new ComplexNumber(5.5, 0);
        ComplexNumber second = new ComplexNumber(1, 0.68);

        ComplexNumber result = first.power(second);
        ComplexNumber expected = new ComplexNumber(2.200255960621758, 5.040721546341201);

        Assert.assertEquals(expected, result);
    }

    @org.junit.Test
    public void testComplexToComplexPower() {
        ComplexNumber first = new ComplexNumber(3.5, 4);
        ComplexNumber second = new ComplexNumber(0.25, -0.75);

        ComplexNumber result = first.power(second);
        ComplexNumber expected = new ComplexNumber(1.4563989345112265, -2.48067918070894);

        Assert.assertEquals(expected, result);
    }

    @org.junit.Test
    public void testNegativeToIntegerPower() {
        ComplexNumber first = new ComplexNumber(-5, 0);
        ComplexNumber second = new ComplexNumber(2, 0);

        ComplexNumber result = first.power(second);
        ComplexNumber expected = new ComplexNumber(25, 0);

        Assert.assertEquals(expected, result);
        Assert.assertEquals(0, result.im, 0.0);
    }

    @org.junit.Test
    public void testNegativeToFractionalPower() {
        ComplexNumber first = new ComplexNumber(-1, 0);
        ComplexNumber second = new ComplexNumber(0.5, 0);

        ComplexNumber result = first.power(second);
        ComplexNumber expected = new ComplexNumber(0, 1);

        Assert.assertEquals(expected, result);
    }


    @org.junit.Test
    public void testZeroToRealPositivePower() {
        ComplexNumber first = new ComplexNumber(0, 0);
        ComplexNumber second = new ComplexNumber(128, 0);

        ComplexNumber result = first.power(second);
        ComplexNumber expected = new ComplexNumber(0, 0);

        Assert.assertEquals(expected, result);
    }

    @org.junit.Test(expected = ComplexNumberException.class)
    public void testZeroToNegativePower() {
        ComplexNumber first = new ComplexNumber(0, 0);
        ComplexNumber second = new ComplexNumber(-5, 0);
        try {
            first.power(second);
        } catch (ComplexNumberException e) {
            Assert.assertTrue(e.getMessage().contains("power"));
            throw e;
        }
    }

    @org.junit.Test(expected = ComplexNumberException.class)
    public void testZeroToComplexPower() {
        ComplexNumber first = new ComplexNumber(0, 0);
        ComplexNumber second = new ComplexNumber(1, 0.0001);
        try {
            first.power(second);
        } catch (ComplexNumberException e) {
            Assert.assertTrue(e.getMessage().contains("power"));
            throw e;
        }
    }

    @org.junit.Test(expected = ComplexNumberException.class)
    public void testPowerOverflow() {
        ComplexNumber first = new ComplexNumber(5, 0);
        ComplexNumber second = new ComplexNumber(2e300, 0);
        try {
            first.power(second);
        } catch (ComplexNumberException e) {
            Assert.assertTrue(e.getMessage().contains("power"));
            throw e;
        }
    }
}
