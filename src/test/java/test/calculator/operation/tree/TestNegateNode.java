package test.calculator.operation.tree;

import calculator.operation.tree.NegateNode;
import calculator.operation.tree.NumberNode;
import calculator.operation.tree.VariableNode;
import calculator.exception.PatternNotRecognizedException;
import calculator.exception.VariableNotFoundException;
import complex.number.ComplexNumber;
import org.junit.Assert;

import java.util.HashMap;
import java.util.Optional;

public class TestNegateNode {
    @org.junit.Test
    public void testCreationCorrect() {
        NegateNode node = NegateNode.Builder
                .getInstance()
                .build(0, "-");

        Assert.assertNotNull(node);
    }

    @org.junit.Test(expected = PatternNotRecognizedException.class)
    public void testCreationIncorrect() {
        NegateNode.Builder
                .getInstance()
                .build(0, "+");
    }

    @org.junit.Test
    public void testEvaluate() {
        NegateNode parent = NegateNode.Builder
                .getInstance()
                .build(0, "-");
        NumberNode child = NumberNode.Builder
                .getInstance()
                .build(0, "5.24i");

        parent.setChild(child);

        ComplexNumber expected = new ComplexNumber(0, -5.24);

        Assert.assertEquals(expected, parent.evaluate());
    }

    @org.junit.Test
    public void testReplaceVariables() throws VariableNotFoundException {
        HashMap<String, ComplexNumber> map = new HashMap<>();
        String name = "var";
        ComplexNumber value = new ComplexNumber(5.2,-1);
        map.put(name, value);

        NegateNode parent = NegateNode.Builder
                .getInstance()
                .build(0, "-");
        VariableNode child = VariableNode.Builder
                .getInstance()
                .build(0, "var");

        parent.setChild(child);

        ComplexNumber expected = new ComplexNumber(-5.2, 1);

        Assert.assertEquals(expected, parent.replaceVariables(map).evaluate());
    }

    @org.junit.Test
    public void testPriority() {
        int initialPriority = 5;

        NegateNode node = NegateNode.Builder
                .getInstance()
                .build(initialPriority, "-");
        int expected = NegateNode.BASIC_PRIORITY + initialPriority;

        Assert.assertEquals(expected, node.getPriority());
    }

    @org.junit.Test
    public void testFindInExpressionCorrect() {
        Optional<String> result = NegateNode.Builder
                .getInstance()
                .findMatchAtExpressionStart("- x+1");
        String expected = "-";

        Assert.assertTrue(result.isPresent());
        Assert.assertEquals(expected, result.get());
    }

    @org.junit.Test
    public void testFindInExpressionIncorrect() {
        Optional<String> result = NegateNode.Builder
                .getInstance()
                .findMatchAtExpressionStart("+6*k");

        Assert.assertFalse(result.isPresent());
    }
}
