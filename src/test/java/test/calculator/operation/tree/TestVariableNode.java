package test.calculator.operation.tree;

import calculator.exception.VariableNotFoundException;
import calculator.exception.OperationNotSupportedException;
import calculator.exception.PatternNotRecognizedException;
import calculator.operation.tree.ValueNode;
import calculator.operation.tree.VariableNode;
import complex.number.ComplexNumber;
import org.junit.Assert;

import java.util.HashMap;
import java.util.Optional;

public class TestVariableNode {
    @org.junit.Test
    public void testCreationOnlyLetters() {
        VariableNode node = VariableNode.Builder
                .getInstance()
                .build(0, "vaRiAbLe");

        Assert.assertNotNull(node);
    }

    @org.junit.Test
    public void testCreationLettersAndNumbers() {
        VariableNode node = VariableNode.Builder
                .getInstance()
                .build(0, "A546mmo");

        Assert.assertNotNull(node);
    }

    @org.junit.Test(expected = PatternNotRecognizedException.class)
    public void testCreationIncorrectNotLetterNumber() {
        VariableNode.Builder
                .getInstance()
                .build(0, "ann*ty");
    }

    @org.junit.Test(expected = PatternNotRecognizedException.class)
    public void testCreationIncorrectLeadingNumber() {
        VariableNode.Builder
                .getInstance()
                .build(0, "4var");
    }

    @org.junit.Test
    public void testReplaceVariables() throws VariableNotFoundException {
        HashMap<String, ComplexNumber> map = new HashMap<>();
        String name = "var5";
        ComplexNumber value = new ComplexNumber(10,-5);
        map.put(name, value);

        ComplexNumber result = VariableNode.Builder
                .getInstance()
                .build(0, name)
                .replaceVariables(map)
                .evaluate();

        Assert.assertEquals(value, result);
    }

    @org.junit.Test(expected = VariableNotFoundException.class)
    public void testReplaceVariablesNotPresent() throws VariableNotFoundException {
        HashMap<String, ComplexNumber> map = new HashMap<>();
        String name = "abc";
        ComplexNumber value = new ComplexNumber(0,0);
        map.put(name, value);

        try {
            VariableNode.Builder
                    .getInstance()
                    .build(0, "abb")
                    .replaceVariables(map);
        } catch (VariableNotFoundException e) {
            Assert.assertTrue(e.getMessage().contains("abb"));
            throw e;
        }
    }

    @org.junit.Test(expected = OperationNotSupportedException.class)
    public void testEvaluate() {
        VariableNode.Builder
                .getInstance()
                .build(0, "a")
                .evaluate();
    }

    @org.junit.Test
    public void testPriority() {
        int initialPriority = 34;

        VariableNode node = VariableNode.Builder
                .getInstance()
                .build(initialPriority, "qwerty");
        int expected = ValueNode.BASIC_PRIORITY + initialPriority;

        Assert.assertEquals(expected, node.getPriority());
    }

    @org.junit.Test
    public void testFindInExpressionCorrect() {
        Optional<String> result = VariableNode.Builder
                .getInstance()
                .findMatchAtExpressionStart("arta5*12");
        String expected = "arta5";

        Assert.assertTrue(result.isPresent());
        Assert.assertEquals(expected, result.get());
    }

    @org.junit.Test
    public void testFindInExpressionIncorrect() {
        Optional<String> result = VariableNode.Builder
                .getInstance()
                .findMatchAtExpressionStart("2mert");

        Assert.assertFalse(result.isPresent());
    }
}
