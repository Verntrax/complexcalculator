package test.calculator.operation.tree;

import calculator.operation.tree.NumberNode;
import calculator.exception.PatternNotRecognizedException;
import calculator.operation.tree.ValueNode;
import complex.number.ComplexNumber;
import org.junit.Assert;

import java.util.HashMap;
import java.util.Optional;

public class TestNumberNode {
    @org.junit.Test
    public void testCreationReal() {
        NumberNode node = NumberNode.Builder
                .getInstance()
                .build(0, "5.467");
        ComplexNumber expected = new ComplexNumber(5.467, 0);

        Assert.assertEquals(expected, node.evaluate());
    }

    @org.junit.Test
    public void testCreationImaginary() {
        NumberNode node = NumberNode.Builder
                .getInstance()
                .build(0, "0.234i");
        ComplexNumber expected = new ComplexNumber(0, 0.234);

        Assert.assertEquals(expected, node.evaluate());
    }

    @org.junit.Test
    public void testCreationZero() {
        NumberNode node = NumberNode.Builder
                .getInstance()
                .build(0, "0");
        ComplexNumber expected = new ComplexNumber(0, 0);

        Assert.assertEquals(expected, node.evaluate());
    }

    @org.junit.Test
    public void testReplaceVariables() {
        NumberNode node = NumberNode.Builder
                .getInstance()
                .build(0, "20i");
        NumberNode other = node.replaceVariables(new HashMap<>());

        Assert.assertSame(node, other);
    }

    @org.junit.Test
    public void testPriority() {
        int initialPriority = 12;

        NumberNode node = NumberNode.Builder
                .getInstance()
                .build(initialPriority, "3i");
        int expected = ValueNode.BASIC_PRIORITY + initialPriority;

        Assert.assertEquals(expected, node.getPriority());
    }

    @org.junit.Test
    public void testFindInExpressionCorrect() {
        Optional<String> result = NumberNode.Builder
                .getInstance()
                .findMatchAtExpressionStart("10.46i*12");
        String expected = "10.46i";

        Assert.assertTrue(result.isPresent());
        Assert.assertEquals(expected, result.get());
    }

    @org.junit.Test
    public void testFindInExpressionIncorrect() {
        Optional<String> result = NumberNode.Builder
                .getInstance()
                .findMatchAtExpressionStart("a23.15");

        Assert.assertFalse(result.isPresent());
    }

    @org.junit.Test(expected = PatternNotRecognizedException.class)
    public void testIncorrectInputLetters() {
        NumberNode.Builder
                .getInstance()
                .build(0, "2.56a91");
    }

    @org.junit.Test(expected = PatternNotRecognizedException.class)
    public void testIncorrectInputLeadingZero() {
        NumberNode.Builder
                .getInstance()
                .build(0, "03");
    }

    @org.junit.Test(expected = PatternNotRecognizedException.class)
    public void testIncorrectInputMultipleDots() {
        NumberNode.Builder
                .getInstance()
                .build(0, "0.3.567");
    }
}
