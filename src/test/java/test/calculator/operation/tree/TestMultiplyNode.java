package test.calculator.operation.tree;

import calculator.exception.VariableNotFoundException;
import calculator.exception.PatternNotRecognizedException;
import calculator.operation.tree.NumberNode;
import calculator.operation.tree.MultiplyNode;
import calculator.operation.tree.VariableNode;
import complex.number.ComplexNumber;
import org.junit.Assert;

import java.util.HashMap;
import java.util.Optional;

public class TestMultiplyNode {
    @org.junit.Test
    public void testCreationCorrect() {
        MultiplyNode node = MultiplyNode.Builder
                .getInstance()
                .build(0, "*");

        Assert.assertNotNull(node);
    }

    @org.junit.Test(expected = PatternNotRecognizedException.class)
    public void testCreationIncorrect() {
        MultiplyNode.Builder
                .getInstance()
                .build(0, "+");
    }

    @org.junit.Test
    public void testEvaluate() {
        MultiplyNode parent = MultiplyNode.Builder
                .getInstance()
                .build(0, "*");
        NumberNode leftChild = NumberNode.Builder
                .getInstance()
                .build(0, "3i");
        NumberNode rightChild = NumberNode.Builder
                .getInstance()
                .build(0, "5i");

        parent.setLeftChild(leftChild);
        parent.setRightChild(rightChild);

        ComplexNumber expected = new ComplexNumber(-15, 0);

        Assert.assertEquals(expected, parent.evaluate());
    }

    @org.junit.Test
    public void testReplaceVariables() throws VariableNotFoundException {
        HashMap<String, ComplexNumber> map = new HashMap<>();
        String name = "var";
        ComplexNumber value = new ComplexNumber(2,-1);
        map.put(name, value);

        MultiplyNode parent = MultiplyNode.Builder
                .getInstance()
                .build(0, "*");
        VariableNode leftChild = VariableNode.Builder
                .getInstance()
                .build(0, "var");
        NumberNode rightChild = NumberNode.Builder
                .getInstance()
                .build(0, "2i");

        parent.setLeftChild(leftChild);
        parent.setRightChild(rightChild);

        ComplexNumber expected = new ComplexNumber(2, 4);

        Assert.assertEquals(expected, parent.replaceVariables(map).evaluate());
    }

    @org.junit.Test
    public void testPriority() {
        int initialPriority = 10;

        MultiplyNode node = MultiplyNode.Builder
                .getInstance()
                .build(initialPriority, "*");
        int expected = MultiplyNode.BASIC_PRIORITY + initialPriority;

        Assert.assertEquals(expected, node.getPriority());
    }

    @org.junit.Test
    public void testFindInExpressionCorrect() {
        Optional<String> result = MultiplyNode.Builder
                .getInstance()
                .findMatchAtExpressionStart("*ans");
        String expected = "*";

        Assert.assertTrue(result.isPresent());
        Assert.assertEquals(expected, result.get());
    }

    @org.junit.Test
    public void testFindInExpressionIncorrect() {
        Optional<String> result = MultiplyNode.Builder
                .getInstance()
                .findMatchAtExpressionStart("10-15");

        Assert.assertFalse(result.isPresent());
    }
}
