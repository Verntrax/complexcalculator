package test.calculator.operation.tree;

import calculator.exception.VariableNotFoundException;
import calculator.exception.PatternNotRecognizedException;
import calculator.operation.tree.NumberNode;
import calculator.operation.tree.PlusNode;
import calculator.operation.tree.VariableNode;
import complex.number.ComplexNumber;
import org.junit.Assert;

import java.util.HashMap;
import java.util.Optional;

public class TestPlusNode {
    @org.junit.Test
    public void testCreationCorrect() {
        PlusNode node = PlusNode.Builder
                .getInstance()
                .build(0, "+");

        Assert.assertNotNull(node);
    }

    @org.junit.Test(expected = PatternNotRecognizedException.class)
    public void testCreationIncorrect() {
        PlusNode.Builder
                .getInstance()
                .build(0, "-");
    }

    @org.junit.Test
    public void testEvaluate() {
        PlusNode parent = PlusNode.Builder
                .getInstance()
                .build(0, "+");
        NumberNode leftChild = NumberNode.Builder
                .getInstance()
                .build(0, "5i");
        NumberNode rightChild = NumberNode.Builder
                .getInstance()
                .build(0, "2");

        parent.setLeftChild(leftChild);
        parent.setRightChild(rightChild);

        ComplexNumber expected = new ComplexNumber(2, 5);

        Assert.assertEquals(expected, parent.evaluate());
    }

    @org.junit.Test
    public void testReplaceVariables() throws VariableNotFoundException {
        HashMap<String, ComplexNumber> map = new HashMap<>();
        String name = "var";
        ComplexNumber value = new ComplexNumber(10,-5);
        map.put(name, value);

        PlusNode parent = PlusNode.Builder
                .getInstance()
                .build(0, "+");
        VariableNode leftChild = VariableNode.Builder
                .getInstance()
                .build(0, "var");
        NumberNode rightChild = NumberNode.Builder
                .getInstance()
                .build(0, "3");

        parent.setLeftChild(leftChild);
        parent.setRightChild(rightChild);

        ComplexNumber expected = new ComplexNumber(13, -5);

        Assert.assertEquals(expected, parent.replaceVariables(map).evaluate());
    }

    @org.junit.Test
    public void testPriority() {
        int initialPriority = 15;

        PlusNode node = PlusNode.Builder
                .getInstance()
                .build(initialPriority, "+");
        int expected = PlusNode.BASIC_PRIORITY + initialPriority;

        Assert.assertEquals(expected, node.getPriority());
    }

    @org.junit.Test
    public void testFindInExpressionCorrect() {
        Optional<String> result = PlusNode.Builder
                .getInstance()
                .findMatchAtExpressionStart("+23");
        String expected = "+";

        Assert.assertTrue(result.isPresent());
        Assert.assertEquals(expected, result.get());
    }

    @org.junit.Test
    public void testFindInExpressionIncorrect() {
        Optional<String> result = PlusNode.Builder
                .getInstance()
                .findMatchAtExpressionStart("-arta");

        Assert.assertFalse(result.isPresent());
    }
}
