package test.calculator.operation.tree;

import calculator.exception.VariableNotFoundException;
import calculator.exception.PatternNotRecognizedException;
import calculator.operation.tree.MinusNode;
import calculator.operation.tree.NumberNode;
import calculator.operation.tree.VariableNode;
import complex.number.ComplexNumber;
import org.junit.Assert;

import java.util.HashMap;
import java.util.Optional;

public class TestMinusNode {
    @org.junit.Test
    public void testCreationCorrect() {
        MinusNode node = MinusNode.Builder
                .getInstance()
                .build(0, "-");

        Assert.assertNotNull(node);
    }

    @org.junit.Test(expected = PatternNotRecognizedException.class)
    public void testCreationIncorrect() {
        MinusNode.Builder
                .getInstance()
                .build(0, "*");
    }

    @org.junit.Test
    public void testEvaluate() {
        MinusNode parent = MinusNode.Builder
                .getInstance()
                .build(0, "-");
        NumberNode leftChild = NumberNode.Builder
                .getInstance()
                .build(0, "2i");
        NumberNode rightChild = NumberNode.Builder
                .getInstance()
                .build(0, "10i");

        parent.setLeftChild(leftChild);
        parent.setRightChild(rightChild);

        ComplexNumber expected = new ComplexNumber(0, -8);

        Assert.assertEquals(expected, parent.evaluate());
    }

    @org.junit.Test
    public void testReplaceVariables() throws VariableNotFoundException {
        HashMap<String, ComplexNumber> map = new HashMap<>();
        String name = "var";
        ComplexNumber value = new ComplexNumber(3,1);
        map.put(name, value);

        MinusNode parent = MinusNode.Builder
                .getInstance()
                .build(0, "-");
        NumberNode leftChild = NumberNode.Builder
                .getInstance()
                .build(0, "2i");
        VariableNode rightChild = VariableNode.Builder
                .getInstance()
                .build(0, "var");

        parent.setLeftChild(leftChild);
        parent.setRightChild(rightChild);

        ComplexNumber expected = new ComplexNumber(-3, 1);

        Assert.assertEquals(expected, parent.replaceVariables(map).evaluate());
    }

    @org.junit.Test
    public void testPriority() {
        int initialPriority = 2;

        MinusNode node = MinusNode.Builder
                .getInstance()
                .build(initialPriority, "-");
        int expected = MinusNode.BASIC_PRIORITY + initialPriority;

        Assert.assertEquals(expected, node.getPriority());
    }

    @org.junit.Test
    public void testFindInExpressionCorrect() {
        Optional<String> result = MinusNode.Builder
                .getInstance()
                .findMatchAtExpressionStart("-abc");
        String expected = "-";

        Assert.assertTrue(result.isPresent());
        Assert.assertEquals(expected, result.get());
    }

    @org.junit.Test
    public void testFindInExpressionIncorrect() {
        Optional<String> result = MinusNode.Builder
                .getInstance()
                .findMatchAtExpressionStart("3*5");

        Assert.assertFalse(result.isPresent());
    }
}
