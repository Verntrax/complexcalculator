package test.calculator.operation.tree;

import calculator.operation.tree.OperationTreeBuilder;
import calculator.operation.tree.*;
import complex.number.ComplexNumber;
import org.junit.Assert;

import java.util.ArrayList;
import java.util.List;

public class TestOperationTreeBuilder {
    @org.junit.Test
    public void testEmptyList() {
        List<CalculationNode> nodes = new ArrayList<>();

        OperationTreeBuilder builder = new OperationTreeBuilder();
        CalculationNode result = builder.build(nodes);

        Assert.assertNull(result);
    }

    @org.junit.Test
    public void testSingleNode() {
        List<CalculationNode> nodes = new ArrayList<>();
        CalculationNode node = NumberNode.Builder
                .getInstance()
                .build(0, "5i");
        nodes.add(node);

        OperationTreeBuilder builder = new OperationTreeBuilder();
        CalculationNode result = builder.build(nodes);

        Assert.assertEquals(result, node);
    }

    @org.junit.Test
    public void testLeftUnaryOperator() {
        List<CalculationNode> nodes = new ArrayList<>();
        nodes.add(NegateNode.Builder
                .getInstance()
                .build(0, "-"));
        nodes.add(NumberNode.Builder
                .getInstance()
                .build(0, "2.5"));

        OperationTreeBuilder builder = new OperationTreeBuilder();
        CalculationNode result = builder.build(nodes);

        ComplexNumber expected = new ComplexNumber(-2.5,0);

        Assert.assertEquals(expected, result.evaluate());
    }

    @org.junit.Test
    public void testRightUnaryOperator() {
        List<CalculationNode> nodes = new ArrayList<>();
        nodes.add(NumberNode.Builder
                .getInstance()
                .build(0, "5i"));
        nodes.add(ConjugateNode.Builder
                .getInstance()
                .build(0, "*"));

        OperationTreeBuilder builder = new OperationTreeBuilder();
        CalculationNode result = builder.build(nodes);

        ComplexNumber expected = new ComplexNumber(0,-5);

        Assert.assertEquals(expected, result.evaluate());
    }

    @org.junit.Test
    public void testBinaryOperator() {
        List<CalculationNode> nodes = new ArrayList<>();
        nodes.add(NumberNode.Builder
                .getInstance()
                .build(0, "2"));
        nodes.add(PlusNode.Builder
                .getInstance()
                .build(0, "+"));
        nodes.add(NumberNode.Builder
                .getInstance()
                .build(0, "5i"));

        OperationTreeBuilder builder = new OperationTreeBuilder();
        CalculationNode result = builder.build(nodes);

        ComplexNumber expected = new ComplexNumber(2,5);

        Assert.assertEquals(expected, result.evaluate());
    }

    @org.junit.Test
    public void testCalculationsSequence() {
        List<CalculationNode> nodes = new ArrayList<>();
        nodes.add(NumberNode.Builder
                .getInstance()
                .build(0, "2"));
        nodes.add(DivideNode.Builder
                .getInstance()
                .build(0, "/"));
        nodes.add(NumberNode.Builder
                .getInstance()
                .build(0, "2"));
        nodes.add(MultiplyNode.Builder
                .getInstance()
                .build(0, "*"));
        nodes.add(NumberNode.Builder
                .getInstance()
                .build(0, "2"));

        OperationTreeBuilder builder = new OperationTreeBuilder();
        CalculationNode result = builder.build(nodes);

        ComplexNumber expected = new ComplexNumber(2,0);

        Assert.assertEquals(expected, result.evaluate());
    }

    @org.junit.Test
    public void testMultipleNodesZeroPriority() {
        List<CalculationNode> nodes = new ArrayList<>();
        nodes.add(NegateNode.Builder
                .getInstance()
                .build(0, "-"));
        nodes.add(NumberNode.Builder
                .getInstance()
                .build(0, "2i"));
        nodes.add(PlusNode.Builder
                .getInstance()
                .build(0, "+"));
        nodes.add(NumberNode.Builder
                .getInstance()
                .build(0, "7"));
        nodes.add(MultiplyNode.Builder
                .getInstance()
                .build(0, "*"));
        nodes.add(NumberNode.Builder
                .getInstance()
                .build(0, "4i"));
        nodes.add(ConjugateNode.Builder
                .getInstance()
                .build(0, "*"));
        nodes.add(MinusNode.Builder
                .getInstance()
                .build(0, "-"));
        nodes.add(NumberNode.Builder
                .getInstance()
                .build(0, "2"));

        OperationTreeBuilder builder = new OperationTreeBuilder();
        CalculationNode result = builder.build(nodes);

        ComplexNumber expected = new ComplexNumber(-2,-30);

        Assert.assertEquals(expected, result.evaluate());
    }

    @org.junit.Test
    public void testMultipleNodesNonZeroPriority() {
        List<CalculationNode> nodes = new ArrayList<>();
        nodes.add(NumberNode.Builder
                .getInstance()
                .build(0, "25"));
        nodes.add(DivideNode.Builder
                .getInstance()
                .build(0, "/"));
        nodes.add(NumberNode.Builder
                .getInstance()
                .build(1000, "3"));
        nodes.add(PlusNode.Builder
                .getInstance()
                .build(1000, "+"));
        nodes.add(NumberNode.Builder
                .getInstance()
                .build(1000, "4i"));
        nodes.add(ConjugateNode.Builder
                .getInstance()
                .build(0, "*"));


        OperationTreeBuilder builder = new OperationTreeBuilder();
        CalculationNode result = builder.build(nodes);

        ComplexNumber expected = new ComplexNumber(3,4);

        Assert.assertEquals(expected, result.evaluate());
    }
}
