package test.calculator.operation.tree;

import calculator.exception.VariableNotFoundException;
import calculator.exception.PatternNotRecognizedException;
import calculator.operation.tree.NumberNode;
import calculator.operation.tree.DivideNode;
import calculator.operation.tree.VariableNode;
import complex.number.ComplexNumber;
import org.junit.Assert;

import java.util.HashMap;
import java.util.Optional;

public class TestDivideNode {
    @org.junit.Test
    public void testCreationCorrect() {
        DivideNode node = DivideNode.Builder
                .getInstance()
                .build(0, "/");

        Assert.assertNotNull(node);
    }

    @org.junit.Test(expected = PatternNotRecognizedException.class)
    public void testCreationIncorrect() {
        DivideNode.Builder
                .getInstance()
                .build(0, "5");
    }

    @org.junit.Test
    public void testEvaluate() {
        DivideNode parent = DivideNode.Builder
                .getInstance()
                .build(0, "/");
        NumberNode leftChild = NumberNode.Builder
                .getInstance()
                .build(0, "10i");
        NumberNode rightChild = NumberNode.Builder
                .getInstance()
                .build(0, "2i");

        parent.setLeftChild(leftChild);
        parent.setRightChild(rightChild);

        ComplexNumber expected = new ComplexNumber(5, 0);

        Assert.assertEquals(expected, parent.evaluate());
    }

    @org.junit.Test
    public void testReplaceVariables() throws VariableNotFoundException {
        HashMap<String, ComplexNumber> map = new HashMap<>();
        String name = "var";
        ComplexNumber value = new ComplexNumber(3,-4);
        map.put(name, value);

        DivideNode parent = DivideNode.Builder
                .getInstance()
                .build(0, "/");
        NumberNode leftChild = NumberNode.Builder
                .getInstance()
                .build(0, "5");
        VariableNode rightChild = VariableNode.Builder
                .getInstance()
                .build(0, "var");

        parent.setLeftChild(leftChild);
        parent.setRightChild(rightChild);

        ComplexNumber expected = new ComplexNumber(0.6, 0.8);

        Assert.assertEquals(expected, parent.replaceVariables(map).evaluate());
    }

    @org.junit.Test
    public void testPriority() {
        int initialPriority = 15;

        DivideNode node = DivideNode.Builder
                .getInstance()
                .build(initialPriority, "/");
        int expected = DivideNode.BASIC_PRIORITY + initialPriority;

        Assert.assertEquals(expected, node.getPriority());
    }

    @org.junit.Test
    public void testFindInExpressionCorrect() {
        Optional<String> result = DivideNode.Builder
                .getInstance()
                .findMatchAtExpressionStart("/-33");
        String expected = "/";

        Assert.assertTrue(result.isPresent());
        Assert.assertEquals(expected, result.get());
    }

    @org.junit.Test
    public void testFindInExpressionIncorrect() {
        Optional<String> result = DivideNode.Builder
                .getInstance()
                .findMatchAtExpressionStart("2*ans");

        Assert.assertFalse(result.isPresent());
    }
}
