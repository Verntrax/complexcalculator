package test.calculator.operation.tree;

import calculator.exception.PatternNotRecognizedException;
import calculator.exception.VariableNotFoundException;
import calculator.operation.tree.NumberNode;
import calculator.operation.tree.PowerNode;
import calculator.operation.tree.VariableNode;
import complex.number.ComplexNumber;
import org.junit.Assert;

import java.util.HashMap;
import java.util.Optional;

public class TestPowerNode {
    @org.junit.Test
    public void testCreationCorrect() {
        PowerNode node = PowerNode.Builder
                .getInstance()
                .build(0, "^");

        Assert.assertNotNull(node);
    }

    @org.junit.Test(expected = PatternNotRecognizedException.class)
    public void testCreationIncorrect() {
        PowerNode.Builder
                .getInstance()
                .build(0, "*");
    }

    @org.junit.Test
    public void testEvaluate() {
        PowerNode parent = PowerNode.Builder
                .getInstance()
                .build(0, "^");
        NumberNode leftChild = NumberNode.Builder
                .getInstance()
                .build(0, "1i");
        NumberNode rightChild = NumberNode.Builder
                .getInstance()
                .build(0, "2");

        parent.setLeftChild(leftChild);
        parent.setRightChild(rightChild);

        ComplexNumber expected = new ComplexNumber(-1, 0);

        Assert.assertEquals(expected, parent.evaluate());
    }

    @org.junit.Test
    public void testReplaceVariables() throws VariableNotFoundException {
        HashMap<String, ComplexNumber> map = new HashMap<>();
        String name = "var";
        ComplexNumber value = new ComplexNumber(2,0);
        map.put(name, value);

        PowerNode parent = PowerNode.Builder
                .getInstance()
                .build(0, "^");
        NumberNode leftChild = NumberNode.Builder
                .getInstance()
                .build(0, "3i");
        VariableNode rightChild = VariableNode.Builder
                .getInstance()
                .build(0, "var");

        parent.setLeftChild(leftChild);
        parent.setRightChild(rightChild);

        ComplexNumber expected = new ComplexNumber(-9, 0);

        Assert.assertEquals(expected, parent.replaceVariables(map).evaluate());
    }

    @org.junit.Test
    public void testPriority() {
        int initialPriority = 15;

        PowerNode node = PowerNode.Builder
                .getInstance()
                .build(initialPriority, "^");
        int expected = PowerNode.BASIC_PRIORITY + initialPriority;

        Assert.assertEquals(expected, node.getPriority());
    }

    @org.junit.Test
    public void testFindInExpressionCorrect() {
        Optional<String> result = PowerNode.Builder
                .getInstance()
                .findMatchAtExpressionStart("^13");
        String expected = "^";

        Assert.assertTrue(result.isPresent());
        Assert.assertEquals(expected, result.get());
    }

    @org.junit.Test
    public void testFindInExpressionIncorrect() {
        Optional<String> result = PowerNode.Builder
                .getInstance()
                .findMatchAtExpressionStart("-12");

        Assert.assertFalse(result.isPresent());
    }
}
