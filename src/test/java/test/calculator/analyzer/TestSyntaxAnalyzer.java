package test.calculator.analyzer;

import calculator.analyzer.AnalyzedStorage;
import calculator.analyzer.SyntaxAnalyzer;
import calculator.exception.InvalidBuilderStateException;
import calculator.exception.SyntaxError;
import calculator.exception.VariableNotFoundException;
import calculator.operation.tree.*;
import complex.number.ComplexNumber;
import org.junit.Assert;

import java.util.HashMap;
import java.util.Map;

public class TestSyntaxAnalyzer {
    @org.junit.Test(expected = InvalidBuilderStateException.class)
    public void testDoubleBuild() {
        SyntaxAnalyzer.Builder builder = new SyntaxAnalyzer.Builder();

        builder.build();
        builder.build();
    }

    @org.junit.Test
    public void testSingleNumber() throws SyntaxError {
        SyntaxAnalyzer.Builder analyzerBuilder = new SyntaxAnalyzer.Builder();
        analyzerBuilder.addBuilder(NumberNode.Builder.getInstance());
        analyzerBuilder.addBuilder(VariableNode.Builder.getInstance());

        SyntaxAnalyzer analyzer = analyzerBuilder.build();

        AnalyzedStorage storage = analyzer.analyze("23i");
        ComplexNumber expected = new ComplexNumber(0, 23);

        Assert.assertFalse(storage.hasAssignment());
        Assert.assertEquals(1, storage.nodes.size());
        Assert.assertEquals(expected, storage.nodes.get(0).evaluate());
    }

    @org.junit.Test
    public void testSingleVariable() throws SyntaxError, VariableNotFoundException {
        SyntaxAnalyzer.Builder analyzerBuilder = new SyntaxAnalyzer.Builder();
        analyzerBuilder.addBuilder(NumberNode.Builder.getInstance());
        analyzerBuilder.addBuilder(VariableNode.Builder.getInstance());

        SyntaxAnalyzer analyzer = analyzerBuilder.build();

        Map<String, ComplexNumber> map = new HashMap<>();
        ComplexNumber value = new ComplexNumber(12, -5);
        String name = "var";
        map.put(name, value);

        AnalyzedStorage storage = analyzer.analyze(name);

        Assert.assertFalse(storage.hasAssignment());
        Assert.assertEquals(1, storage.nodes.size());
        Assert.assertEquals(value, storage.nodes.get(0).replaceVariables(map).evaluate());
    }

    @org.junit.Test
    public void testAssignment() throws SyntaxError {
        SyntaxAnalyzer.Builder analyzerBuilder = new SyntaxAnalyzer.Builder();
        analyzerBuilder.addBuilder(NumberNode.Builder.getInstance());
        analyzerBuilder.addBuilder(VariableNode.Builder.getInstance());

        SyntaxAnalyzer analyzer = analyzerBuilder.build();

        AnalyzedStorage storage = analyzer.analyze("var = 2.34");
        ComplexNumber expected = new ComplexNumber(2.34, 0);

        Assert.assertTrue(storage.hasAssignment());
        Assert.assertEquals("var", storage.assignmentName);
        Assert.assertEquals(1, storage.nodes.size());
        Assert.assertEquals(expected, storage.nodes.get(0).evaluate());
    }

    @org.junit.Test
    public void testBinaryOperation() throws SyntaxError {
        SyntaxAnalyzer.Builder analyzerBuilder = new SyntaxAnalyzer.Builder();
        analyzerBuilder.addBuilder(NumberNode.Builder.getInstance());
        analyzerBuilder.addBuilder(VariableNode.Builder.getInstance());

        analyzerBuilder.addBuilder(PlusNode.Builder.getInstance());
        analyzerBuilder.addBuilder(MultiplyNode.Builder.getInstance());

        SyntaxAnalyzer analyzer = analyzerBuilder.build();

        AnalyzedStorage storage = analyzer.analyze("var = 2 * 3i");

        Assert.assertTrue(storage.hasAssignment());
        Assert.assertEquals("var", storage.assignmentName);
        Assert.assertEquals(3, storage.nodes.size());

        Object[] expected = {
                NumberNode.class,
                MultiplyNode.class,
                NumberNode.class
        };
        Object[] result = storage.nodes
                .stream()
                .map(Object::getClass)
                .toArray();

        Assert.assertArrayEquals(expected, result);
    }

    @org.junit.Test
    public void testLeftUnaryOperation() throws SyntaxError {
        SyntaxAnalyzer.Builder analyzerBuilder = new SyntaxAnalyzer.Builder();
        analyzerBuilder.addBuilder(NumberNode.Builder.getInstance());
        analyzerBuilder.addBuilder(VariableNode.Builder.getInstance());

        analyzerBuilder.addBuilder(NegateNode.Builder.getInstance());

        SyntaxAnalyzer analyzer = analyzerBuilder.build();

        AnalyzedStorage storage = analyzer.analyze("- 5");

        Assert.assertFalse(storage.hasAssignment());
        Assert.assertEquals(2, storage.nodes.size());

        Object[] expected = {
                NegateNode.class,
                NumberNode.class
        };
        Object[] result = storage.nodes
                .stream()
                .map(Object::getClass)
                .toArray();

        Assert.assertArrayEquals(expected, result);
    }

    @org.junit.Test
    public void testRightUnaryOperation() throws SyntaxError {
        SyntaxAnalyzer.Builder analyzerBuilder = new SyntaxAnalyzer.Builder();
        analyzerBuilder.addBuilder(NumberNode.Builder.getInstance());
        analyzerBuilder.addBuilder(VariableNode.Builder.getInstance());

        analyzerBuilder.addBuilder(ConjugateNode.Builder.getInstance());

        SyntaxAnalyzer analyzer = analyzerBuilder.build();

        AnalyzedStorage storage = analyzer.analyze("2i*");

        Assert.assertFalse(storage.hasAssignment());
        Assert.assertEquals(2, storage.nodes.size());

        Object[] expected = {
                NumberNode.class,
                ConjugateNode.class
        };
        Object[] result = storage.nodes
                .stream()
                .map(Object::getClass)
                .toArray();

        Assert.assertArrayEquals(expected, result);
    }

    @org.junit.Test(expected = SyntaxError.class)
    public void testEmptyBrackets() throws SyntaxError {
        SyntaxAnalyzer.Builder analyzerBuilder = new SyntaxAnalyzer.Builder();
        analyzerBuilder.addBuilder(NumberNode.Builder.getInstance());
        analyzerBuilder.addBuilder(VariableNode.Builder.getInstance());

        SyntaxAnalyzer analyzer = analyzerBuilder.build();

        analyzer.analyze("var = ()");
    }

    @org.junit.Test(expected = SyntaxError.class)
    public void testSplitVariable() throws SyntaxError {
        SyntaxAnalyzer.Builder analyzerBuilder = new SyntaxAnalyzer.Builder();
        analyzerBuilder.addBuilder(NumberNode.Builder.getInstance());
        analyzerBuilder.addBuilder(VariableNode.Builder.getInstance());

        SyntaxAnalyzer analyzer = analyzerBuilder.build();

        analyzer.analyze("va r");
    }

    @org.junit.Test(expected = SyntaxError.class)
    public void testSplitNumber() throws SyntaxError {
        SyntaxAnalyzer.Builder analyzerBuilder = new SyntaxAnalyzer.Builder();
        analyzerBuilder.addBuilder(NumberNode.Builder.getInstance());
        analyzerBuilder.addBuilder(VariableNode.Builder.getInstance());

        SyntaxAnalyzer analyzer = analyzerBuilder.build();

        analyzer.analyze("3 i");
    }

    @org.junit.Test(expected = SyntaxError.class)
    public void testEmptyAssignment() throws SyntaxError {
        SyntaxAnalyzer.Builder analyzerBuilder = new SyntaxAnalyzer.Builder();
        analyzerBuilder.addBuilder(NumberNode.Builder.getInstance());
        analyzerBuilder.addBuilder(VariableNode.Builder.getInstance());

        SyntaxAnalyzer analyzer = analyzerBuilder.build();

        analyzer.analyze("var = ");
    }

    @org.junit.Test
    public void testSpacesEverywhere() throws SyntaxError {
        SyntaxAnalyzer.Builder analyzerBuilder = new SyntaxAnalyzer.Builder();
        analyzerBuilder.addBuilder(NumberNode.Builder.getInstance());
        analyzerBuilder.addBuilder(VariableNode.Builder.getInstance());

        analyzerBuilder.addBuilder(MultiplyNode.Builder.getInstance());

        SyntaxAnalyzer analyzer = analyzerBuilder.build();

        AnalyzedStorage storage = analyzer.analyze("  var  = 2 * ( 5i * 1i ) ");

        Assert.assertTrue(storage.hasAssignment());
        Assert.assertEquals(5, storage.nodes.size());
    }

    @org.junit.Test
    public void testBrackets() throws SyntaxError {
        SyntaxAnalyzer.Builder analyzerBuilder = new SyntaxAnalyzer.Builder();
        analyzerBuilder.addBuilder(NumberNode.Builder.getInstance());
        analyzerBuilder.addBuilder(VariableNode.Builder.getInstance());

        analyzerBuilder.addBuilder(PlusNode.Builder.getInstance());
        analyzerBuilder.addBuilder(MultiplyNode.Builder.getInstance());
        analyzerBuilder.addBuilder(ConjugateNode.Builder.getInstance());

        SyntaxAnalyzer analyzer = analyzerBuilder.build();

        AnalyzedStorage storage = analyzer.analyze("2 * (3 + 2i)*");

        Assert.assertFalse(storage.hasAssignment());
        Assert.assertEquals(6, storage.nodes.size());

        Object[] expected = {
                ValueNode.BASIC_PRIORITY,
                MultiplyNode.BASIC_PRIORITY,
                ValueNode.BASIC_PRIORITY + SyntaxAnalyzer.BRACKET_PRIORITY,
                PlusNode.BASIC_PRIORITY + SyntaxAnalyzer.BRACKET_PRIORITY,
                ValueNode.BASIC_PRIORITY + SyntaxAnalyzer.BRACKET_PRIORITY,
                ConjugateNode.BASIC_PRIORITY
        };
        Object[] result = storage.nodes
                .stream()
                .map(CalculationNode::getPriority)
                .toArray();

        Assert.assertArrayEquals(expected, result);
    }

    @org.junit.Test
    public void testTabs() throws SyntaxError {
        SyntaxAnalyzer.Builder analyzerBuilder = new SyntaxAnalyzer.Builder();
        analyzerBuilder.addBuilder(NumberNode.Builder.getInstance());
        analyzerBuilder.addBuilder(VariableNode.Builder.getInstance());

        analyzerBuilder.addBuilder(NegateNode.Builder.getInstance());
        analyzerBuilder.addBuilder(MultiplyNode.Builder.getInstance());

        SyntaxAnalyzer analyzer = analyzerBuilder.build();

        AnalyzedStorage storage = analyzer.analyze("\t\tvar\t=-3i\t\t*\t6\t\t");

        Assert.assertTrue(storage.hasAssignment());
        Assert.assertEquals("var", storage.assignmentName);
        Assert.assertEquals(4, storage.nodes.size());
    }

    @org.junit.Test
    public void testComplexExpression() throws SyntaxError {
        SyntaxAnalyzer.Builder analyzerBuilder = new SyntaxAnalyzer.Builder();
        analyzerBuilder.addBuilder(NumberNode.Builder.getInstance());
        analyzerBuilder.addBuilder(VariableNode.Builder.getInstance());

        analyzerBuilder.addBuilder(NegateNode.Builder.getInstance());
        analyzerBuilder.addBuilder(ConjugateNode.Builder.getInstance());
        analyzerBuilder.addBuilder(PlusNode.Builder.getInstance());
        analyzerBuilder.addBuilder(MinusNode.Builder.getInstance());
        analyzerBuilder.addBuilder(MultiplyNode.Builder.getInstance());
        analyzerBuilder.addBuilder(DivideNode.Builder.getInstance());
        analyzerBuilder.addBuilder(PowerNode.Builder.getInstance());

        SyntaxAnalyzer analyzer = analyzerBuilder.build();

        AnalyzedStorage storage = analyzer.analyze("var=-ans*(b33*2/8i)*((-2i+5.22)**8.3i^2)");

        Assert.assertTrue(storage.hasAssignment());
        Assert.assertEquals("var", storage.assignmentName);
        Assert.assertEquals(18, storage.nodes.size());
    }

    @org.junit.Test
    public void testTwoExpressions() throws SyntaxError {
        SyntaxAnalyzer.Builder analyzerBuilder = new SyntaxAnalyzer.Builder();
        analyzerBuilder.addBuilder(NumberNode.Builder.getInstance());
        analyzerBuilder.addBuilder(VariableNode.Builder.getInstance());

        analyzerBuilder.addBuilder(NegateNode.Builder.getInstance());
        analyzerBuilder.addBuilder(ConjugateNode.Builder.getInstance());
        analyzerBuilder.addBuilder(PlusNode.Builder.getInstance());
        analyzerBuilder.addBuilder(MinusNode.Builder.getInstance());
        analyzerBuilder.addBuilder(MultiplyNode.Builder.getInstance());
        analyzerBuilder.addBuilder(DivideNode.Builder.getInstance());

        SyntaxAnalyzer analyzer = analyzerBuilder.build();

        AnalyzedStorage storage = analyzer.analyze("-2+5i*8*(((2+2)*3))/8");
        Assert.assertFalse(storage.hasAssignment());
        Assert.assertEquals(14, storage.nodes.size());

        storage = analyzer.analyze("var=0.33i+5i*(a2+3i)*");
        Assert.assertTrue(storage.hasAssignment());
        Assert.assertEquals("var", storage.assignmentName);
        Assert.assertEquals(8, storage.nodes.size());
    }

    @org.junit.Test(expected = SyntaxError.class)
    public void testIncorrectSyntax() throws SyntaxError {
        SyntaxAnalyzer.Builder analyzerBuilder = new SyntaxAnalyzer.Builder();
        analyzerBuilder.addBuilder(NumberNode.Builder.getInstance());
        analyzerBuilder.addBuilder(VariableNode.Builder.getInstance());

        analyzerBuilder.addBuilder(PlusNode.Builder.getInstance());
        analyzerBuilder.addBuilder(MinusNode.Builder.getInstance());
        analyzerBuilder.addBuilder(MultiplyNode.Builder.getInstance());
        analyzerBuilder.addBuilder(DivideNode.Builder.getInstance());

        SyntaxAnalyzer analyzer = analyzerBuilder.build();

        analyzer.analyze("arr*2+3i++9i*ans");
    }

    @org.junit.Test(expected = SyntaxError.class)
    public void testUnknownOperator() throws SyntaxError {
        SyntaxAnalyzer.Builder analyzerBuilder = new SyntaxAnalyzer.Builder();
        analyzerBuilder.addBuilder(NumberNode.Builder.getInstance());
        analyzerBuilder.addBuilder(VariableNode.Builder.getInstance());

        analyzerBuilder.addBuilder(PlusNode.Builder.getInstance());

        SyntaxAnalyzer analyzer = analyzerBuilder.build();

        analyzer.analyze("arr+2i*3");
    }

    @org.junit.Test(expected = SyntaxError.class)
    public void testUnbalancedBrackets() throws SyntaxError {
        SyntaxAnalyzer.Builder analyzerBuilder = new SyntaxAnalyzer.Builder();
        analyzerBuilder.addBuilder(NumberNode.Builder.getInstance());
        analyzerBuilder.addBuilder(VariableNode.Builder.getInstance());

        analyzerBuilder.addBuilder(PlusNode.Builder.getInstance());
        analyzerBuilder.addBuilder(MinusNode.Builder.getInstance());
        analyzerBuilder.addBuilder(MultiplyNode.Builder.getInstance());
        analyzerBuilder.addBuilder(DivideNode.Builder.getInstance());

        SyntaxAnalyzer analyzer = analyzerBuilder.build();

        analyzer.analyze("2*((3i+8)/2i");
    }

    @org.junit.Test(expected = SyntaxError.class)
    public void testBadlyOrderedBrackets() throws SyntaxError {
        SyntaxAnalyzer.Builder analyzerBuilder = new SyntaxAnalyzer.Builder();
        analyzerBuilder.addBuilder(NumberNode.Builder.getInstance());
        analyzerBuilder.addBuilder(VariableNode.Builder.getInstance());

        analyzerBuilder.addBuilder(PlusNode.Builder.getInstance());
        analyzerBuilder.addBuilder(MinusNode.Builder.getInstance());
        analyzerBuilder.addBuilder(MultiplyNode.Builder.getInstance());
        analyzerBuilder.addBuilder(DivideNode.Builder.getInstance());

        SyntaxAnalyzer analyzer = analyzerBuilder.build();

        analyzer.analyze("var=12)*3+(8i");
    }

    @org.junit.Test(expected = SyntaxError.class)
    public void testMisplacedBrackets() throws SyntaxError {
        SyntaxAnalyzer.Builder analyzerBuilder = new SyntaxAnalyzer.Builder();
        analyzerBuilder.addBuilder(NumberNode.Builder.getInstance());
        analyzerBuilder.addBuilder(VariableNode.Builder.getInstance());

        analyzerBuilder.addBuilder(PlusNode.Builder.getInstance());
        analyzerBuilder.addBuilder(MinusNode.Builder.getInstance());
        analyzerBuilder.addBuilder(MultiplyNode.Builder.getInstance());
        analyzerBuilder.addBuilder(DivideNode.Builder.getInstance());

        SyntaxAnalyzer analyzer = analyzerBuilder.build();

        analyzer.analyze("var=3i(*2+12)/6i");
    }
}
