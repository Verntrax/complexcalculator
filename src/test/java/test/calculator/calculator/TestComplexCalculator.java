package test.calculator.calculator;

import calculator.exception.SyntaxError;
import calculator.calculator.ComplexCalculator;
import calculator.exception.InvalidBuilderStateException;
import calculator.exception.VariableNotFoundException;
import calculator.operation.tree.*;
import complex.number.ComplexNumber;
import org.junit.Assert;

import java.util.Map;

public class TestComplexCalculator {
    @org.junit.Test(expected = InvalidBuilderStateException.class)
    public void testDoubleBuild() {
        ComplexCalculator.Builder builder = new ComplexCalculator.Builder();

        builder.build();
        builder.build();
    }

    @org.junit.Test
    public void testSanity() throws VariableNotFoundException, SyntaxError {
        ComplexCalculator.Builder builder = new ComplexCalculator.Builder();

        ComplexCalculator calculator = builder.build();
        ComplexNumber result = calculator.calculate("5i");

        ComplexNumber expected = new ComplexNumber(0, 5);

        Assert.assertEquals(expected, result);
    }

    @org.junit.Test
    public void testAssignment() throws VariableNotFoundException, SyntaxError {
        ComplexCalculator.Builder builder = new ComplexCalculator.Builder();

        ComplexCalculator calculator = builder.build();
        ComplexNumber result = calculator.calculate("ans = 12.56");
        Map<String, ComplexNumber> vars = calculator.getVariables();

        ComplexNumber expected = new ComplexNumber(12.56, 0);

        Assert.assertEquals(expected, result);
        Assert.assertEquals(1, vars.size());
        Assert.assertTrue(vars.containsKey("ans"));
        Assert.assertEquals(expected, vars.get("ans"));
    }

    @org.junit.Test
    public void testComplexExpression() throws VariableNotFoundException, SyntaxError {
        ComplexCalculator.Builder builder = new ComplexCalculator.Builder();
        builder.addOperatorBuilder(NegateNode.Builder.getInstance());
        builder.addOperatorBuilder(ConjugateNode.Builder.getInstance());
        builder.addOperatorBuilder(PlusNode.Builder.getInstance());
        builder.addOperatorBuilder(MinusNode.Builder.getInstance());
        builder.addOperatorBuilder(MultiplyNode.Builder.getInstance());
        builder.addOperatorBuilder(DivideNode.Builder.getInstance());
        builder.addOperatorBuilder(PowerNode.Builder.getInstance());

        ComplexCalculator calculator = builder.build();
        ComplexNumber result = calculator.calculate("((-(2i-4)*6+10/2i)*)^(0.5i+1)");

        ComplexNumber expected = new ComplexNumber(-14.511215019123544, 16.014770925708603);

        Assert.assertEquals(expected, result);
    }

    @org.junit.Test
    public void testMultipleBrackets() throws VariableNotFoundException, SyntaxError {
        ComplexCalculator.Builder builder = new ComplexCalculator.Builder();
        builder.addOperatorBuilder(NegateNode.Builder.getInstance());
        builder.addOperatorBuilder(ConjugateNode.Builder.getInstance());
        builder.addOperatorBuilder(PlusNode.Builder.getInstance());

        ComplexCalculator calculator = builder.build();
        ComplexNumber result = calculator.calculate("-(-(-((3+2i)*))*)");

        ComplexNumber expected = new ComplexNumber(-3, -2);

        Assert.assertEquals(expected, result);
    }

    @org.junit.Test
    public void testClearVariable() throws VariableNotFoundException, SyntaxError {
        ComplexCalculator.Builder builder = new ComplexCalculator.Builder();

        ComplexCalculator calculator = builder.build();
        calculator.calculate("a=3i");
        calculator.calculate("b=12");
        calculator.clearVariable("b");

        Map<String, ComplexNumber> vars = calculator.getVariables();

        Assert.assertEquals(1, vars.size());
    }

    @org.junit.Test
    public void testClearVariables() throws VariableNotFoundException, SyntaxError {
        ComplexCalculator.Builder builder = new ComplexCalculator.Builder();

        ComplexCalculator calculator = builder.build();
        calculator.calculate("a=33");
        calculator.calculate("b=1i");
        calculator.clearVariables();

        Map<String, ComplexNumber> vars = calculator.getVariables();

        Assert.assertEquals(0, vars.size());
    }

    @org.junit.Test(expected = SyntaxError.class)
    public void testSyntaxError() throws VariableNotFoundException, SyntaxError {
        ComplexCalculator.Builder builder = new ComplexCalculator.Builder();
        builder.addOperatorBuilder(PlusNode.Builder.getInstance());
        builder.addOperatorBuilder(MultiplyNode.Builder.getInstance());

        ComplexCalculator calculator = builder.build();
        calculator.calculate("12i + 6 * 8 i");
    }

    @org.junit.Test(expected = VariableNotFoundException.class)
    public void testVariableNotFound() throws VariableNotFoundException, SyntaxError {
        ComplexCalculator.Builder builder = new ComplexCalculator.Builder();
        builder.addOperatorBuilder(PlusNode.Builder.getInstance());
        builder.addOperatorBuilder(MultiplyNode.Builder.getInstance());

        ComplexCalculator calculator = builder.build();
        calculator.calculate("a=18i+6");
        calculator.calculate("b=3*a+2i");
        try {
            calculator.calculate("a+b+var*b");
        } catch (VariableNotFoundException e) {
            Assert.assertTrue(e.getMessage().contains("var"));
            throw e;
        }
    }

    @org.junit.Test
    public void testAssignmentSeries() throws VariableNotFoundException, SyntaxError {
        ComplexCalculator.Builder builder = new ComplexCalculator.Builder();
        builder.addOperatorBuilder(PlusNode.Builder.getInstance());
        builder.addOperatorBuilder(MinusNode.Builder.getInstance());
        builder.addOperatorBuilder(MultiplyNode.Builder.getInstance());
        builder.addOperatorBuilder(DivideNode.Builder.getInstance());

        ComplexCalculator calculator = builder.build();
        calculator.calculate("a=12i+5*(2i+1)");
        calculator.calculate("b=4i-a");
        calculator.calculate("a=2");

        Map<String, ComplexNumber> vars = calculator.getVariables();

        ComplexNumber expectedA = new ComplexNumber(2, 0);
        ComplexNumber expectedB = new ComplexNumber(-5, -18);

        Assert.assertEquals(2, vars.size());
        Assert.assertTrue(vars.containsKey("a"));
        Assert.assertEquals(expectedA, vars.get("a"));
        Assert.assertTrue(vars.containsKey("b"));
        Assert.assertEquals(expectedB, vars.get("b"));
    }
}
