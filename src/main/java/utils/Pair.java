package utils;

/**
 * Template for storing two Objects of the same class.
 * @param <T> class of stored objects
 */
public class Pair<T> {
    /**
     * First element of this pair.
     */
    public final T first;
    /**
     * Second element of this pair.
     */
    public final T second;

    /**
     * Group given objects in a pair.
     * @param first first element of the new pair
     * @param second second element of the new pair
     */
    public Pair(T first, T second) {
        this.first = first;
        this.second = second;
    }
}
