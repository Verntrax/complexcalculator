package utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class meant to store all methods commonly
 * used in the project.
 */
public class Utils {
    private static final Pattern whitespace = Pattern.compile("\\h+");

    private Utils() {}

    /**
     * Removes all whitespaces from the front of
     * the given {@link String}.
     * @param expression {@link String} to be processed
     * @return given argument without any leading whitespaces
     */
    public static String removeLeadingSpaces(String expression) {
        Matcher matcher = whitespace.matcher(expression);
        if (matcher.lookingAt())
            expression = expression.substring(matcher.group(0).length());
        return expression;
    }
}
