package calculator.exception;

/**
 * Exception thrown if {@link calculator.operation.tree.CalculationNode}
 * is attempted to be built from invalid source.
 */
public class PatternNotRecognizedException extends RuntimeException {}
