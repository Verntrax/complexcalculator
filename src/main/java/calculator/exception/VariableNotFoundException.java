package calculator.exception;

/**
 * Exception thrown as a result of attempt of
 * variable replacement which is not present in
 * a given variable dictionary.
 */
public class VariableNotFoundException extends Exception {
    /**
     * Creates exception holding the given message.
     * @param message message informing which variable
     *                was not found
     */
    public VariableNotFoundException(String message) {
        super(message);
    }
}
