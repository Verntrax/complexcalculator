package calculator.exception;

/**
 * Exception thrown if {@link calculator.analyzer.SyntaxAnalyzer}
 * detects errors in analyzed expression.
 */
public class SyntaxError extends Exception {}
