package calculator.exception;

/**
 * Exception thrown every time builder class
 * is in incorrect state while attempting to build
 * an object.
 */
public class InvalidBuilderStateException extends RuntimeException {}
