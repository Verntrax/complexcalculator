package calculator.exception;

/**
 * Exception thrown when overridden method should not
 * be called for the specific class.
 */
public class OperationNotSupportedException extends RuntimeException {}
