package calculator.calculator;

import calculator.analyzer.AnalyzedStorage;
import calculator.operation.tree.OperationTreeBuilder;
import calculator.analyzer.SyntaxAnalyzer;
import calculator.exception.SyntaxError;
import calculator.exception.InvalidBuilderStateException;
import calculator.exception.VariableNotFoundException;
import calculator.operation.tree.*;
import complex.number.ComplexNumber;

import java.util.*;

/**
 * Calculator evaluating mathematical expressions
 * involving complex numbers. Calculator stores
 * values of assigned variables used during calculations.
 */
public class ComplexCalculator {
    private final SyntaxAnalyzer analyzer;
    private final OperationTreeBuilder treeBuilder;
    private final Map<String, ComplexNumber> varDict;

    /**
     * Builder class for the {@link ComplexCalculator}.
     */
    public static class Builder {
        private final SyntaxAnalyzer.Builder analyzerBuilder;

        /**
         * Creates {@link ComplexCalculator.Builder} object.
         */
        public Builder() {
            analyzerBuilder = new SyntaxAnalyzer.Builder();

            analyzerBuilder.addBuilder(NumberNode.Builder.getInstance());
            analyzerBuilder.addBuilder(VariableNode.Builder.getInstance());
        }

        /**
         * Adds {@link BinaryOperatorNode.Builder} which
         * should be used in the built {@link ComplexCalculator}.
         * @param builder {@link BinaryOperatorNode.Builder} added
         */
        public void addOperatorBuilder(BinaryOperatorNode.Builder builder) {
            analyzerBuilder.addBuilder(builder);
        }

        /**
         * Adds {@link LeftUnaryOperatorNode.Builder} which
         * should be used in the built {@link ComplexCalculator}.
         * @param builder {@link LeftUnaryOperatorNode.Builder} added
         */
        public void addOperatorBuilder(LeftUnaryOperatorNode.Builder builder) {
            analyzerBuilder.addBuilder(builder);
        }

        /**
         * Adds {@link RightUnaryOperatorNode.Builder} which
         * should be used in the built {@link ComplexCalculator}.
         * @param builder {@link RightUnaryOperatorNode.Builder} added
         */
        public void addOperatorBuilder(RightUnaryOperatorNode.Builder builder) {
            analyzerBuilder.addBuilder(builder);
        }

        /**
         * Creates {@link ComplexCalculator} with previously added
         * operators.
         * @return new {@link ComplexCalculator}
         * @throws InvalidBuilderStateException if build method is called
         * more than once for this builder
         */
        public ComplexCalculator build() throws InvalidBuilderStateException {
            SyntaxAnalyzer analyzer = analyzerBuilder.build();
            return new ComplexCalculator(analyzer);
        }
    }

    private ComplexCalculator(SyntaxAnalyzer analyzer) {
        this.analyzer = analyzer;
        treeBuilder = new OperationTreeBuilder();
        varDict = new HashMap<>();
    }

    /**
     * Analyzes and evaluates a given mathematical expression.
     * If the expression consists of the assignment part,
     * result of the expression is stored under the name given
     * in the expression. All operators given to this {@link ComplexCalculator}
     * can be used in the expression.
     * @param expression mathematical expression to be
     *                   evaluated
     * @return result of the expression evaluation
     * @throws VariableNotFoundException if the variable which
     * was not assigned before is used in the expression
     * @throws SyntaxError if syntax of the expression is
     * invalid (e.g. brackets are not balanced)
     */
    public ComplexNumber calculate(String expression) throws VariableNotFoundException, SyntaxError {
        AnalyzedStorage analyzed = analyzer.analyze(expression);

        CalculationNode operationTree = treeBuilder.build(analyzed.nodes);
        operationTree = operationTree.replaceVariables(varDict);

        ComplexNumber result = operationTree.evaluate();

        if (analyzed.hasAssignment())
            varDict.put(analyzed.assignmentName, result);
        return result;
    }

    /**
     * Gives immutable dictionary of variables
     * and their assigned values.
     * @return dictionary of stored variables
     */
    public Map<String, ComplexNumber> getVariables() {
        return Collections.unmodifiableMap(varDict);
    }

    /**
     * Removes given variable from the calculator
     * memory.
     * @param name name of the variable to be erased
     */
    public void clearVariable(String name) {
        varDict.remove(name);
    }

    /**
     * Removes all variables from the calculator memory.
     */
    public void clearVariables() {
        varDict.clear();
    }
}
