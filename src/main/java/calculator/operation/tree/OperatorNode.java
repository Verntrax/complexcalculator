package calculator.operation.tree;

/**
 * Class representing all operator nodes -
 * both unary and binary.
 */
public abstract class OperatorNode extends CalculationNode {

    OperatorNode() {}

    /**
     * Builder class for the {@link OperatorNode}.
     */
    public static abstract class Builder extends CalculationNode.Builder {
        Builder(String regex) {
            super(regex);
        }
    }
}
