package calculator.operation.tree;

import calculator.exception.PatternNotRecognizedException;
import complex.number.ComplexNumber;

/**
 * Class representing power operator in
 * the expression.
 */
public class PowerNode extends BinaryOperatorNode {
    /**
     * Basic priority of power operator node.
     */
    public final static int BASIC_PRIORITY = 3;
    /**
     * Regular expression matching power operator.
     */
    public final static String REGEX = "\\^";

    private final int priority;

    /**
     * Builder class for the {@link PowerNode}.
     * This class implements a singleton pattern.
     */
    public static class Builder extends BinaryOperatorNode.Builder {
        private static final PowerNode.Builder instance = new PowerNode.Builder();

        /**
         * Retrieves instance of {@link PowerNode.Builder}.
         * @return instance of {@link PowerNode.Builder}
         */
        public static PowerNode.Builder getInstance() {
            return instance;
        }

        private Builder() {
            super(REGEX);
        }

        /**
         * Builds {@link PowerNode} based on the given source
         * {@link String}. {@link PatternNotRecognizedException}
         * is thrown if source does not match the {@link PowerNode#REGEX}.
         * @param initialPriority externally provided base priority
         *                        (brackets based)
         * @param source {@link String} to be converted to node
         * @return created {@link PowerNode}
         */
        @Override
        public PowerNode build(int initialPriority, String source) {
            if (!validate(source))
                throw new PatternNotRecognizedException();

            int priority = initialPriority + BASIC_PRIORITY;
            return new PowerNode(priority);
        }
    }

    private PowerNode(int priority) {
        this.priority = priority;
    }

    @Override
    ComplexNumber operation(ComplexNumber left, ComplexNumber right) {
        return left.power(right);
    }

    /**
     * Gives priority of this node.
     * @return priority of this node
     */
    @Override
    public int getPriority() {
        return priority;
    }
}
