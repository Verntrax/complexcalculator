package calculator.operation.tree;

import calculator.exception.VariableNotFoundException;
import calculator.exception.OperationNotSupportedException;
import calculator.exception.PatternNotRecognizedException;
import complex.number.ComplexNumber;

import java.util.Map;

/**
 * Class representing variables in the expression.
 * All variables needs to be replaced with their
 * numerical values before computation.
 */
public class VariableNode extends ValueNode {
    /**
     * Regular expression matching variables names.
     */
    public final static String REGEX = "[a-zA-Z][a-zA-Z0-9]*";

    private final String name;
    private final int priority;

    /**
     * Builder class for the {@link VariableNode}.
     * This class implements a singleton pattern.
     */
    public static class Builder extends ValueNode.Builder {
        private static final Builder instance = new Builder();

        /**
         * Retrieves instance of {@link ValueNode.Builder}.
         * @return instance of {@link VariableNode.Builder}
         */
        public static Builder getInstance() {
            return instance;
        }

        private Builder() {
            super(REGEX);
        }

        /**
         * Builds {@link VariableNode} based on the given source
         * {@link String}. {@link PatternNotRecognizedException}
         * is thrown if source does not match the {@link VariableNode#REGEX}.
         * @param initialPriority externally provided base priority
         *                        (brackets based)
         * @param source {@link String} to be converted to node
         * @return {@link VariableNode} created based on a given
         * source {@link String}
         */
        @Override
        public VariableNode build(int initialPriority, String source) {
            if (!validate(source))
                throw new PatternNotRecognizedException();

            int priority = initialPriority + BASIC_PRIORITY;
            return new VariableNode(priority, source);
        }
    }

    private VariableNode(int priority, String name) {
        this.priority = priority;
        this.name = name;
    }

    /**
     * Throws {@link OperationNotSupportedException}.
     * {@link VariableNode} cannot be evaluated. It needs
     * to be replaced with its numerical value first.
     * @return nothing
     */
    @Override
    public ComplexNumber evaluate() {
        throw new OperationNotSupportedException();
    }

    /**
     * Replaces variable  represented by this node with
     * its numerical values.
     * @param dict dictionary of variables storing name-value
     *             pairs
     * @return {@link NumberNode} storing numerical value
     * of replaced variable
     * @throws VariableNotFoundException if variable present
     * in this node is not found in the dictionary
     */
    @Override
    public NumberNode replaceVariables(Map<String, ComplexNumber> dict) throws VariableNotFoundException {
        ComplexNumber value = dict.get(name);

        if (value == null)
            throw new VariableNotFoundException(name);

        return NumberNode.Builder.getInstance().build(priority, value);
    }

    /**
     * Gives priority of this node.
     * @return priority of this node
     */
    @Override
    public int getPriority() {
        return priority;
    }
}
