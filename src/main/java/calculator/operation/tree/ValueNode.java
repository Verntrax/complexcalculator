package calculator.operation.tree;

import java.util.List;

/**
 * Class representing all nodes storing numerical values.
 */
public abstract class ValueNode extends CalculationNode {
    /**
     * Basic priority of all value nodes.
     */
    public static final int BASIC_PRIORITY = 100;

    ValueNode() {}

    /**
     * Builder class for the {@link ValueNode}.
     */
    public static abstract class Builder extends CalculationNode.Builder {
        Builder(String regex) {
            super(regex);
        }
    }

    @Override
    void visit(OperationTreeBuilder builder, List<CalculationNode> nodes) {
        builder.accept(this, nodes);
    }
}
