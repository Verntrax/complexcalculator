package calculator.operation.tree;

import calculator.exception.PatternNotRecognizedException;
import complex.number.ComplexNumber;

/**
 * Class representing subtraction operator in
 * the expression.
 */
public class MinusNode extends BinaryOperatorNode {
    /**
     * Basic priority of subtract operator node.
     */
    public final static int BASIC_PRIORITY = 1;
    /**
     * Regular expression matching subtraction operator.
     */
    public final static String REGEX = "-";

    private final int priority;

    /**
     * Builder class for the {@link MinusNode}.
     * This class implements a singleton pattern.
     */
    public static class Builder extends BinaryOperatorNode.Builder {
        private static final MinusNode.Builder instance = new MinusNode.Builder();

        /**
         * Retrieves instance of {@link MinusNode.Builder}.
         * @return instance of {@link MinusNode.Builder}
         */
        public static MinusNode.Builder getInstance() {
            return instance;
        }

        private Builder() {
            super(REGEX);
        }

        /**
         * Builds {@link MinusNode} based on the given source
         * {@link String}. {@link PatternNotRecognizedException}
         * is thrown if source does not match the {@link MinusNode#REGEX}.
         * @param initialPriority externally provided base priority
         *                        (brackets based)
         * @param source {@link String} to be converted to node
         * @return created {@link MinusNode}
         */
        @Override
        public MinusNode build(int initialPriority, String source) {
            if (!validate(source))
                throw new PatternNotRecognizedException();

            int priority = initialPriority + BASIC_PRIORITY;
            return new MinusNode(priority);
        }
    }

    private MinusNode(int priority) {
        this.priority = priority;
    }

    @Override
    ComplexNumber operation(ComplexNumber left, ComplexNumber right) {
        return left.subtract(right);
    }

    /**
     * Gives priority of this node.
     * @return priority of this node
     */
    @Override
    public int getPriority() {
        return priority;
    }
}