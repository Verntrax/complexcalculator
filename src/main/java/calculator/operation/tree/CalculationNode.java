package calculator.operation.tree;

import calculator.exception.VariableNotFoundException;
import complex.number.ComplexNumber;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class representing a basic unit of math
 * operation tree.
 */
public abstract class CalculationNode {

    CalculationNode() {}

    /**
     * Builder class for the {@link CalculationNode}.
     */
    public static abstract class Builder {
        private final Pattern pattern;

        Builder(String regex) {
            this.pattern = Pattern.compile(regex);
        }

        boolean validate(String source) {
            return pattern.matcher(source)
                    .matches();
        }

        /**
         * Find and extract {@link String} from the beginning of
         * a given expression which matches the pattern specific
         * for the exact calculation node.
         * @param expression expression to be looked into
         * @return Optional storing the longest possible match
         * found at the beginning of expression. If no match was
         * found, Optional is empty
         */
        public Optional<String> findMatchAtExpressionStart(String expression) {
            Matcher matcher = pattern.matcher(expression);

            if (matcher.lookingAt()) {
                return Optional.of(matcher.group(0));
            }
            return Optional.empty();
        }

        /**
         * Build node specific for the Builder
         * @param initialPriority externally provided base priority
         *                        (brackets based)
         * @param source {@link String} to be converted to node
         * @return {@link CalculationNode} specific for the builder
         * created based on the given source {@link String}
         */
        public abstract CalculationNode build(int initialPriority, String source);
    }

    /**
     * Evaluates tree with the root in this node.
     * @return {@link ComplexNumber} being the result
     * of evaluation
     */
    public abstract ComplexNumber evaluate();

    /**
     * Replaces variables with their numerical values
     * in the whole tree rooted in this node.
     * @param dict dictionary of variables storing name-value
     *             pairs
     * @return root of the tree with variables replaced
     * @throws VariableNotFoundException if variable present
     * in the tree is not found in the variable dictionary
     */
    public abstract CalculationNode replaceVariables(Map<String, ComplexNumber> dict) throws VariableNotFoundException;

    /**
     * Gives priority of this node.
     * @return priority of this node
     */
    public abstract int getPriority();

    abstract void visit(OperationTreeBuilder builder, List<CalculationNode> nodes);
}
