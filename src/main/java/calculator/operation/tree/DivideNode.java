package calculator.operation.tree;

import calculator.exception.PatternNotRecognizedException;
import complex.number.ComplexNumber;

/**
 * Class representing division operator in
 * the expression.
 */
public class DivideNode extends BinaryOperatorNode {
    /**
     * Basic priority of divide operator node.
     */
    public final static int BASIC_PRIORITY = 2;
    /**
     * Regular expression matching division operator.
     */
    public final static String REGEX = "/";

    private final int priority;

    /**
     * Builder class for the {@link DivideNode}.
     * This class implements a singleton pattern.
     */
    public static class Builder extends BinaryOperatorNode.Builder {
        private static final DivideNode.Builder instance = new DivideNode.Builder();

        /**
         * Retrieves instance of {@link DivideNode.Builder}.
         * @return instance of {@link DivideNode.Builder}
         */
        public static DivideNode.Builder getInstance() {
            return instance;
        }

        private Builder() {
            super(REGEX);
        }

        /**
         * Builds {@link DivideNode} based on the given source
         * {@link String}. {@link PatternNotRecognizedException}
         * is thrown if source does not match the {@link DivideNode#REGEX}.
         * @param initialPriority externally provided base priority
         *                        (brackets based)
         * @param source {@link String} to be converted to node
         * @return created {@link DivideNode}
         */
        @Override
        public DivideNode build(int initialPriority, String source) {
            if (!validate(source))
                throw new PatternNotRecognizedException();

            int priority = initialPriority + BASIC_PRIORITY;
            return new DivideNode(priority);
        }
    }

    private DivideNode(int priority) {
        this.priority = priority;
    }

    @Override
    ComplexNumber operation(ComplexNumber left, ComplexNumber right) {
        return left.divide(right);
    }

    /**
     * Gives priority of this node.
     * @return priority of this node
     */
    @Override
    public int getPriority() {
        return priority;
    }
}