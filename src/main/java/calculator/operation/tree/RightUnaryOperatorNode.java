package calculator.operation.tree;

import java.util.List;

/**
 * Class representing all right unary operator nodes.
 */
public abstract class RightUnaryOperatorNode extends UnaryOperatorNode {

    RightUnaryOperatorNode() {}

    /**
     * Basic priority of all right unary operator nodes.
     */
    public final static int BASIC_PRIORITY = 50;

    /**
     * Builder class for the {@link RightUnaryOperatorNode}.
     */
    public static abstract class Builder extends UnaryOperatorNode.Builder {
        Builder(String regex) {
            super(regex);
        }
    }

    @Override
    void visit(OperationTreeBuilder builder, List<CalculationNode> nodes) {
        builder.accept(this, nodes);
    }
}
