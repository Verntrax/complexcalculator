package calculator.operation.tree;

import calculator.exception.PatternNotRecognizedException;
import complex.number.ComplexNumber;

/**
 * Class representing addition operator in
 * the expression.
 */
public class PlusNode extends BinaryOperatorNode {
    /**
     * Basic priority of add operator node.
     */
    public final static int BASIC_PRIORITY = 1;
    /**
     * Regular expression matching addition operator.
     */
    public final static String REGEX = "\\+";

    private final int priority;

    /**
     * Builder class for the {@link PlusNode}.
     * This class implements a singleton pattern.
     */
    public static class Builder extends BinaryOperatorNode.Builder {
        private static final PlusNode.Builder instance = new PlusNode.Builder();

        /**
         * Retrieves instance of {@link PlusNode.Builder}.
         * @return instance of {@link PlusNode.Builder}
         */
        public static PlusNode.Builder getInstance() {
            return instance;
        }

        private Builder() {
            super(REGEX);
        }

        /**
         * Builds {@link PlusNode} based on the given source
         * {@link String}. {@link PatternNotRecognizedException}
         * is thrown if source does not match the {@link PlusNode#REGEX}.
         * @param initialPriority externally provided base priority
         *                        (brackets based)
         * @param source {@link String} to be converted to node
         * @return created {@link PlusNode}
         */
        @Override
        public PlusNode build(int initialPriority, String source) {
            if (!validate(source))
                throw new PatternNotRecognizedException();

            int priority = initialPriority + BASIC_PRIORITY;
            return new PlusNode(priority);
        }
    }

    private PlusNode(int priority) {
        this.priority = priority;
    }

    @Override
    ComplexNumber operation(ComplexNumber left, ComplexNumber right) {
        return left.add(right);
    }

    /**
     * Gives priority of this node.
     * @return priority of this node
     */
    @Override
    public int getPriority() {
        return priority;
    }
}
