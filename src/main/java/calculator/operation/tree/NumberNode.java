package calculator.operation.tree;

import calculator.exception.PatternNotRecognizedException;
import complex.number.ComplexNumber;

import java.util.Map;

/**
 * Class representing complex numbers in the expression.
 */
public class NumberNode extends ValueNode {
    /**
     * Regular expression matching complex numbers.
     */
    public final static String REGEX = "(([1-9][0-9]*\\.?[0-9]*)|(0(\\.[0-9]*)?))i?";

    private final ComplexNumber value;
    private final int priority;

    /**
     * Builder class for the {@link NumberNode}.
     * This class implements a singleton pattern.
     */
    public static class Builder extends ValueNode.Builder {
        private static final Builder instance = new Builder();

        /**
         * Retrieves instance of {@link NumberNode.Builder}.
         * @return instance of {@link NumberNode.Builder}
         */
        public static Builder getInstance() {
            return instance;
        }

        private Builder() {
            super(REGEX);
        }

        /**
         * Builds {@link NumberNode} based on the given source
         * {@link String}. {@link PatternNotRecognizedException}
         * is thrown if source does not match the {@link NumberNode#REGEX}.
         * @param initialPriority externally provided base priority
         *                        (brackets based)
         * @param source {@link String} to be converted to node
         * @return {@link NumberNode} created based on a given
         * source {@link String}
         */
        @Override
        public NumberNode build(int initialPriority, String source) {
            if (!validate(source))
                throw new PatternNotRecognizedException();

            ComplexNumber number;
            if (source.endsWith("i")) {
                source = source.substring(0, source.length() - 1);
                double value = Double.valueOf(source);
                number =  new ComplexNumber(0, value);
            } else {
                double value = Double.valueOf(source);
                number = new ComplexNumber(value, 0);
            }
            int priority = initialPriority + BASIC_PRIORITY;
            return new NumberNode(priority, number);
        }

        NumberNode build(int priority, ComplexNumber value) {
            return new NumberNode(priority, value);
        }
    }

    private NumberNode(int priority, ComplexNumber value) {
        this.priority = priority;
        this.value = value;
    }

    /**
     * Gives value stored in this node.
     * @return {@link ComplexNumber} stored in this node
     */
    @Override
    public ComplexNumber evaluate() {
        return this.value;
    }

    /**
     * Gives this node. It does not have any children, so
     * no replacement is performed.
     * @param dict dictionary of variables storing name-value
     *             pairs
     * @return root of the tree with variables replaced
     * in the tree is not found in the variable dictionary
     */
    @Override
    public NumberNode replaceVariables(Map<String, ComplexNumber> dict) {
        return this;
    }

    /**
     * Gives priority of this node.
     * @return priority of this node
     */
    @Override
    public int getPriority() {
        return priority;
    }
}
