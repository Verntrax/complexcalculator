package calculator.operation.tree;

import calculator.exception.PatternNotRecognizedException;
import complex.number.ComplexNumber;

/**
 * Class representing conjugation operator in
 * the expression.
 */
public class ConjugateNode extends RightUnaryOperatorNode {
    /**
     * Regular expression matching conjugation operator.
     */
    public final static String REGEX = "\\*";

    private final int priority;

    /**
     * Builder class for the {@link ConjugateNode}.
     * This class implements a singleton pattern.
     */
    public static class Builder extends RightUnaryOperatorNode.Builder {
        private static final ConjugateNode.Builder instance = new ConjugateNode.Builder();

        /**
         * Retrieves instance of {@link ConjugateNode.Builder}.
         * @return instance of {@link ConjugateNode.Builder}
         */
        public static ConjugateNode.Builder getInstance() {
            return instance;
        }

        private Builder() {
            super(REGEX);
        }

        /**
         * Builds {@link ConjugateNode} based on the given source
         * {@link String}. {@link PatternNotRecognizedException}
         * is thrown if source does not match the {@link ConjugateNode#REGEX}.
         * @param initialPriority externally provided base priority
         *                        (brackets based)
         * @param source {@link String} to be converted to node
         * @return created {@link ConjugateNode}
         */
        @Override
        public ConjugateNode build(int initialPriority, String source) {
            if (!validate(source))
                throw new PatternNotRecognizedException();

            int priority = initialPriority + BASIC_PRIORITY;
            return new ConjugateNode(priority);
        }
    }

    private ConjugateNode(int priority) {
        this.priority = priority;
    }

    @Override
    ComplexNumber operation(ComplexNumber result) {
        return result.conjugate();
    }

    /**
     * Gives priority of this node.
     * @return priority of this node
     */
    @Override
    public int getPriority() {
        return priority;
    }
}
