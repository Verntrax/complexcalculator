package calculator.operation.tree;

import calculator.exception.VariableNotFoundException;
import complex.number.ComplexNumber;

import java.util.Map;

/**
 * Class representing all unary operator nodes.
 */
public abstract class UnaryOperatorNode extends OperatorNode {
    private CalculationNode child;

    UnaryOperatorNode() {}

    /**
     * Builder class for the {@link UnaryOperatorNode}.
     */
    public static abstract class Builder extends OperatorNode.Builder {
        Builder(String regex) {
            super(regex);
        }
    }

    /**
     * Evaluates tree with the root in this node. First this
     * method gets results of the evaluation of its child.
     * Then it uses the results for further computations.
     * @return {@link ComplexNumber} being the result
     * of evaluation
     */
    @Override
    public ComplexNumber evaluate() {
        ComplexNumber childResult = child.evaluate();

        return operation(childResult);
    }

    /**
     * Replaces variables with their numerical value
     * in the whole tree rooted in this node.
     * @param dict dictionary of variables storing name-value
     *             pairs
     * @return root of the tree with variables replaced
     * @throws VariableNotFoundException if variable present
     * in the tree is not found in the variable dictionary
     */
    @Override
    public CalculationNode replaceVariables(Map<String, ComplexNumber> dict) throws VariableNotFoundException {
        child = child.replaceVariables(dict);

        return this;
    }

    /**
     * Sets the only child of this node.
     * @param child node to be assigned this node as its
     *              only child
     */
    public void setChild(CalculationNode child) {
        this.child = child;
    }

    abstract ComplexNumber operation(ComplexNumber result);
}
