package calculator.operation.tree;

import calculator.exception.PatternNotRecognizedException;
import complex.number.ComplexNumber;

/**
 * Class representing negation operator in
 * the expression.
 */
public class NegateNode extends LeftUnaryOperatorNode {
    /**
     * Regular expression matching negation operator.
     */
    public final static String REGEX = "-";

    private final int priority;

    /**
     * Builder class for the {@link NegateNode}.
     * This class implements a singleton pattern.
     */
    public static class Builder extends LeftUnaryOperatorNode.Builder {
        private static final NegateNode.Builder instance = new NegateNode.Builder();

        /**
         * Retrieves instance of {@link NegateNode.Builder}.
         * @return instance of {@link NegateNode.Builder}
         */
        public static NegateNode.Builder getInstance() {
            return instance;
        }

        private Builder() {
            super(REGEX);
        }

        /**
         * Builds {@link NegateNode} based on the given source
         * {@link String}. {@link PatternNotRecognizedException}
         * is thrown if source does not match the {@link NegateNode#REGEX}.
         * @param initialPriority externally provided base priority
         *                        (brackets based)
         * @param source {@link String} to be converted to node
         * @return created {@link NegateNode}
         */
        @Override
        public NegateNode build(int initialPriority, String source) {
            if (!validate(source))
                throw new PatternNotRecognizedException();

            int priority = initialPriority + BASIC_PRIORITY;
            return new NegateNode(priority);
        }
    }

    private NegateNode(int priority) {
        this.priority = priority;
    }

    @Override
    ComplexNumber operation(ComplexNumber result) {
        return result.negate();
    }

    /**
     * Gives priority of this node.
     * @return priority of this node
     */
    @Override
    public int getPriority() {
        return priority;
    }
}
