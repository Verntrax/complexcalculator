package calculator.operation.tree;

import java.util.List;

/**
 * Class representing all left unary operator nodes.
 */
public abstract class LeftUnaryOperatorNode extends UnaryOperatorNode {

    LeftUnaryOperatorNode() {}

    /**
     * Basic priority of all left unary operator nodes.
     */
    public final static int BASIC_PRIORITY = 1;

    /**
     * Builder class for the {@link LeftUnaryOperatorNode}.
     */
    public static abstract class Builder extends UnaryOperatorNode.Builder {
        Builder(String regex) {
            super(regex);
        }
    }

    @Override
    void visit(OperationTreeBuilder builder, List<CalculationNode> nodes) {
        builder.accept(this, nodes);
    }
}
