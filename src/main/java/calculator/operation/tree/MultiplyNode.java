package calculator.operation.tree;

import calculator.exception.PatternNotRecognizedException;
import complex.number.ComplexNumber;

/**
 * Class representing multiplication operator in
 * the expression.
 */
public class MultiplyNode extends BinaryOperatorNode {
    /**
     * Basic priority of multiply operator node.
     */
    public final static int BASIC_PRIORITY = 2;
    /**
     * Regular expression matching multiplication operator.
     */
    public final static String REGEX = "\\*";

    private final int priority;

    /**
     * Builder class for the {@link MultiplyNode}.
     * This class implements a singleton pattern.
     */
    public static class Builder extends BinaryOperatorNode.Builder {
        private static final MultiplyNode.Builder instance = new MultiplyNode.Builder();

        /**
         * Retrieves instance of {@link MultiplyNode.Builder}.
         * @return instance of {@link MultiplyNode.Builder}
         */
        public static MultiplyNode.Builder getInstance() {
            return instance;
        }

        private Builder() {
            super(REGEX);
        }

        /**
         * Builds {@link MultiplyNode} based on the given source
         * {@link String}. {@link PatternNotRecognizedException}
         * is thrown if source does not match the {@link MultiplyNode#REGEX}.
         * @param initialPriority externally provided base priority
         *                        (brackets based)
         * @param source {@link String} to be converted to node
         * @return created {@link MultiplyNode}
         */
        @Override
        public MultiplyNode build(int initialPriority, String source) {
            if (!validate(source))
                throw new PatternNotRecognizedException();

            int priority = initialPriority + BASIC_PRIORITY;
            return new MultiplyNode(priority);
        }
    }

    private MultiplyNode(int priority) {
        this.priority = priority;
    }

    @Override
    ComplexNumber operation(ComplexNumber left, ComplexNumber right) {
        return left.multiply(right);
    }

    /**
     * Gives priority of this node.
     * @return priority of this node
     */
    @Override
    public int getPriority() {
        return priority;
    }
}