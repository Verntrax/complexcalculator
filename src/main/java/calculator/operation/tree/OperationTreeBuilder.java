package calculator.operation.tree;

import java.util.List;

/**
 * Class used for building operation tree, given
 * list of nodes this tree should be built from.
 * It is assumed that the given list describes
 * valid mathematical expression.
 */
public class OperationTreeBuilder {

    /**
     * Builds an operation tree based on a given {@link List}
     * of {@link CalculationNode} describing valid mathematical
     * expression.
     * @param nodes {@link List} of {@link CalculationNode} describing
     *                          valid expression
     * @return root of operation tree
     */
    public CalculationNode build(List<CalculationNode> nodes) {
        if (nodes.isEmpty())
            return null;
        CalculationNode node = findParent(nodes);
        node.visit(this, nodes);
        return node;
    }

    void accept(LeftUnaryOperatorNode parent, List<CalculationNode> nodes) {
        List<CalculationNode> children = nodes.subList(1, nodes.size());

        CalculationNode child = build(children);
        parent.setChild(child);
    }

    void accept(RightUnaryOperatorNode parent, List<CalculationNode> nodes) {
        List<CalculationNode> children = nodes.subList(0, nodes.size() - 1);

        CalculationNode child = build(children);
        parent.setChild(child);
    }

    void accept(BinaryOperatorNode parent, List<CalculationNode> nodes) {
        int index = nodes.indexOf(parent);
        List<CalculationNode> leftNodes = nodes.subList(0, index);
        List<CalculationNode> rightNodes = nodes.subList(index + 1, nodes.size());

        CalculationNode leftChild = build(leftNodes);
        CalculationNode rightChild = build(rightNodes);

        parent.setLeftChild(leftChild);
        parent.setRightChild(rightChild);
    }

    void accept(ValueNode parent, List<CalculationNode> nodes) {}

    private CalculationNode findParent(List<CalculationNode> nodes) {
        int minPriority = Integer.MAX_VALUE;
        CalculationNode result = null;

        for (CalculationNode node : nodes) {
            if (node.getPriority() <= minPriority) {
                result = node;
                minPriority = node.getPriority();
            }
        }
        return result;
    }
}
