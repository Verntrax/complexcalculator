package calculator.operation.tree;

import calculator.exception.VariableNotFoundException;
import complex.number.ComplexNumber;

import java.util.List;
import java.util.Map;

/**
 * Class representing all binary operator nodes.
 */
public abstract class BinaryOperatorNode extends OperatorNode {
    private CalculationNode leftChild;
    private CalculationNode rightChild;

    BinaryOperatorNode() {}

    /**
     * Builder class for the {@link BinaryOperatorNode}.
     */
    public static abstract class Builder extends OperatorNode.Builder {
        Builder(String regex) {
            super(regex);
        }
    }

    /**
     * Evaluates tree with the root in this node. First this
     * method gets results of the evaluation of its children.
     * Then it uses the results for further computations.
     * @return {@link ComplexNumber} being the result
     * of evaluation
     */
    @Override
    public ComplexNumber evaluate() {
        ComplexNumber leftResult = leftChild.evaluate();
        ComplexNumber rightResult = rightChild.evaluate();

        return operation(leftResult, rightResult);
    }

    /**
     * Replaces variables with their numerical value
     * in the whole tree rooted in this node.
     * @param dict dictionary of variables storing name-value
     *             pairs
     * @return root of the tree with variables replaced
     * @throws VariableNotFoundException if variable present
     * in the tree is not found in the variable dictionary
     */
    @Override
    public CalculationNode replaceVariables(Map<String, ComplexNumber> dict) throws VariableNotFoundException {
        leftChild = leftChild.replaceVariables(dict);
        rightChild = rightChild.replaceVariables(dict);

        return this;
    }

    /**
     * Sets left child of this node.
     * @param child node to be assigned this node as its
     *              left child
     */
    public void setLeftChild(CalculationNode child) {
        leftChild = child;
    }

    /**
     * Sets right child of this node.
     * @param child node to be assigned this node as its
     *              right child
     */
    public void setRightChild(CalculationNode child) {
        rightChild = child;
    }

    @Override
    void visit(OperationTreeBuilder builder, List<CalculationNode> nodes) {
        builder.accept(this, nodes);
    }

    abstract ComplexNumber operation(ComplexNumber left, ComplexNumber right);
}
