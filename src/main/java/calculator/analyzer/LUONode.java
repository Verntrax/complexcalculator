package calculator.analyzer;

class LUONode extends AnalyzerTreeNode {
    @Override
    void visit(SyntaxAnalyzer analyzer, String expression, int priority) {
        analyzer.accept(this, expression, priority);
    }
}
