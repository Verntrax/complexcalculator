package calculator.analyzer;

import calculator.exception.SyntaxError;
import calculator.exception.InvalidBuilderStateException;
import calculator.operation.tree.*;
import utils.Pair;
import utils.Utils;

import java.text.MessageFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class for analyzing and parsing mathematical
 * expressions.
 */
public class SyntaxAnalyzer {
    /**
     * Priority boost for every opened bracket in
     * the expression.
     */
    public static final int BRACKET_PRIORITY = 1000;
    /**
     * {@link String} describing opening bracket.
     */
    public static final String OPENING_BRACKET = "(";
    /**
     * {@link String} describing closing bracket.
     */
    public static final String CLOSING_BRACKET = ")";
    /**
     * {@link String} describing assignment sign.
     */
    public static final String ASSIGNMENT = "=";

    private final List<ValueNode.Builder> valueBuilders;
    private final List<BinaryOperatorNode.Builder> binaryOperatorBuilders;
    private final List<LeftUnaryOperatorNode.Builder> leftUnaryOperatorBuilders;
    private final List<RightUnaryOperatorNode.Builder> rightUnaryOperatorBuilders;
    private final Pattern assignmentPattern;
    private final StartNode analyzerTree;

    private List<CalculationNode> nodes;
    private boolean expressionValid;

    /**
     * Builder class for the {@link SyntaxAnalyzer}.
     */
    public static class Builder {
        private final Set<ValueNode.Builder> valueBuildersSet;
        private final Set<BinaryOperatorNode.Builder> binaryOperatorBuildersSet;
        private final Set<LeftUnaryOperatorNode.Builder> leftUnaryOperatorBuildersSet;
        private final Set<RightUnaryOperatorNode.Builder> rightUnaryOperatorBuildersSet;
        private boolean valid;

        /**
         * Creates {@link SyntaxAnalyzer.Builder} object.
         */
        public Builder() {
            valueBuildersSet = new HashSet<>();
            binaryOperatorBuildersSet = new HashSet<>();
            leftUnaryOperatorBuildersSet = new HashSet<>();
            rightUnaryOperatorBuildersSet = new HashSet<>();
            valid = true;
        }

        /**
         * Adds {@link ValueNode.Builder} which
         * should be used in the built {@link SyntaxAnalyzer}.
         * @param builder {@link ValueNode.Builder} added
         */
        public void addBuilder(ValueNode.Builder builder) {
            valueBuildersSet.add(builder);
        }

        /**
         * Adds {@link BinaryOperatorNode.Builder} which
         * should be used in the built {@link SyntaxAnalyzer}.
         * @param builder {@link BinaryOperatorNode.Builder} added
         */
        public void addBuilder(BinaryOperatorNode.Builder builder) {
            binaryOperatorBuildersSet.add(builder);
        }

        /**
         * Adds {@link LeftUnaryOperatorNode.Builder} which
         * should be used in the built {@link SyntaxAnalyzer}.
         * @param builder {@link LeftUnaryOperatorNode.Builder} added
         */
        public void addBuilder(LeftUnaryOperatorNode.Builder builder) {
            leftUnaryOperatorBuildersSet.add(builder);
        }

        /**
         * Adds {@link RightUnaryOperatorNode.Builder} which
         * should be used in the built {@link SyntaxAnalyzer}.
         * @param builder {@link RightUnaryOperatorNode.Builder} added
         */
        public void addBuilder(RightUnaryOperatorNode.Builder builder) {
            rightUnaryOperatorBuildersSet.add(builder);
        }

        /**
         * Creates {@link SyntaxAnalyzer} with previously added
         * values types and operators.
         * @return new {@link SyntaxAnalyzer}
         * @throws InvalidBuilderStateException if build method is called
         * more than once for this builder
         */
        public SyntaxAnalyzer build() throws InvalidBuilderStateException {
            if (!valid)
                throw new InvalidBuilderStateException();

            valid = false;

            return new SyntaxAnalyzer(new ArrayList<>(valueBuildersSet),
                    new ArrayList<>(binaryOperatorBuildersSet),
                    new ArrayList<>(leftUnaryOperatorBuildersSet),
                    new ArrayList<>(rightUnaryOperatorBuildersSet));
        }
    }

    private SyntaxAnalyzer(List<ValueNode.Builder> valueBuilders,
                          List<BinaryOperatorNode.Builder> binaryOperatorBuilders,
                          List<LeftUnaryOperatorNode.Builder> leftUnaryOperatorBuilders,
                          List<RightUnaryOperatorNode.Builder> rightUnaryOperatorBuilders) {
        this.valueBuilders = valueBuilders;
        this.binaryOperatorBuilders = binaryOperatorBuilders;
        this.leftUnaryOperatorBuilders = leftUnaryOperatorBuilders;
        this.rightUnaryOperatorBuilders = rightUnaryOperatorBuilders;

        Object[] params = {
                VariableNode.REGEX,
                ASSIGNMENT
        };
        String regex = MessageFormat.format("\\h*({0})\\h*{1}", params);
        this.assignmentPattern = Pattern.compile(regex);
        this.analyzerTree = AnalyzerTree.getTree();
    }

    /**
     * Analyzes and parses given expression.
     * @param expression expression to be analyzed
     * @return {@link AnalyzedStorage} storing {@link List}
     * of {@link CalculationNode} being the result of parsing
     * and optionally name of the variable result of the
     * expression evaluation should be assigned to.
     * @throws SyntaxError if expression is invalid
     */
    public AnalyzedStorage analyze(String expression) throws SyntaxError {
        initializeAnalyzer();
        Pair<String> splitExpression = splitByAssignmentName(expression);
        buildNodes(splitExpression.second);

        return new AnalyzedStorage(splitExpression.first, nodes);
    }

    void accept(StartNode node, String expression, int priority) {
        analyzeByChildren(node, expression, priority);
    }

    void accept(OpeningBracketNode node, String expression, int priority) {
        if (!expression.startsWith(OPENING_BRACKET))
            return;
        expression = expression.substring(OPENING_BRACKET.length());
        priority += BRACKET_PRIORITY;
        analyzeByChildren(node, expression, priority);
    }

    void accept(ClosingBracketNode node, String expression, int priority) {
        if (!expression.startsWith(CLOSING_BRACKET) || priority < BRACKET_PRIORITY)
            return;
        expression = expression.substring(CLOSING_BRACKET.length());
        priority -= BRACKET_PRIORITY;
        analyzeByChildren(node, expression, priority);
    }

    void accept(BONode node, String expression, int priority) {
        processCalculationNode(binaryOperatorBuilders, node, expression, priority);
    }

    void accept(LUONode node, String expression, int priority) {
        processCalculationNode(leftUnaryOperatorBuilders, node, expression, priority);
    }

    void accept(RUONode node, String expression, int priority) {
        processCalculationNode(rightUnaryOperatorBuilders, node, expression, priority);
    }

    void accept(VNode node, String expression, int priority) {
        processCalculationNode(valueBuilders, node, expression, priority);
    }

    void accept(EndNode node, String expression, int priority) {
        if (expression.equals("") && priority == 0)
            expressionValid = true;
    }

    private void initializeAnalyzer() {
        nodes = new ArrayList<>();
        expressionValid = false;
    }

    private Pair<String> splitByAssignmentName(String expression) {
        String assignmentName = null;

        Matcher matcher = assignmentPattern.matcher(expression);
        if (matcher.lookingAt()) {
            expression = expression.substring(matcher.group(0).length());
            assignmentName = matcher.group(1);
        }
        return new Pair<>(assignmentName, expression);
    }

    private void buildNodes(String expression) throws SyntaxError {
        analyzerTree.visit(this, expression, 0);

        if (!expressionValid)
            throw new SyntaxError();
    }

    private void analyzeByChildren(AnalyzerTreeNode node, String expression, int priority) {
        expression = Utils.removeLeadingSpaces(expression);
        for (AnalyzerTreeNode child : node.getChildren()) {
            child.visit(this, expression, priority);
            if (expressionValid)
                return;
        }
    }

    private void processCalculationNode(List<? extends CalculationNode.Builder> builders,
                                        AnalyzerTreeNode node, String expression, int priority) {
        Optional<String> appendResult = appendMatchingNode(builders, expression, priority);

        appendResult.ifPresent(expr -> {
            analyzeByChildren(node, expr, priority);
            removeNodeIfNotValid();
        });
    }

    private Optional<String> appendMatchingNode(List<? extends CalculationNode.Builder> builders,
                                      String expression, int priority) {
        return builders.stream()
                .map(builder -> this.appendNodeFromExpression(builder, expression, priority))
                .filter(Optional::isPresent)
                .findFirst()
                .flatMap(x -> x);
    }

    private Optional<String> appendNodeFromExpression(CalculationNode.Builder builder,
                                                      String expression, int priority) {
        Optional<String> found = builder.findMatchAtExpressionStart(expression);

        found.ifPresent(matchedExpr -> {
            CalculationNode node = builder.build(priority, matchedExpr);
            nodes.add(node);
        });

        return found.flatMap(matchedExpr -> {
            String exprRest = expression.substring(matchedExpr.length());
            return Optional.of(exprRest);
        });
    }

    private void removeNodeIfNotValid() {
        if (!expressionValid)
            nodes.remove(nodes.size() - 1);
    }
}
