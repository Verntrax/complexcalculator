package calculator.analyzer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

abstract class AnalyzerTreeNode {
    private final List<AnalyzerTreeNode> children;

    AnalyzerTreeNode() {
        children = new ArrayList<>();
    }

    void addChild(AnalyzerTreeNode child) {
        children.add(child);
    }

    List<AnalyzerTreeNode> getChildren() {
        return Collections.unmodifiableList(children);
    }

    abstract void visit(SyntaxAnalyzer analyzer, String expression, int priority);
}
