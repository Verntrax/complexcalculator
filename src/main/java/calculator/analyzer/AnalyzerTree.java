package calculator.analyzer;

class AnalyzerTree {
    private static final StartNode startNode = new StartNode();
    private static final RUONode ruonode = new RUONode();
    private static final LUONode luonode = new LUONode();
    private static final VNode vnode = new VNode();
    private static final BONode bonode = new BONode();
    private static final OpeningBracketNode openingBracketNode = new OpeningBracketNode();
    private static final ClosingBracketNode closingBracketNode = new ClosingBracketNode();
    private static final EndNode endNode = new EndNode();

    private static StartNode analyzerTree;

    private AnalyzerTree() {}

    static StartNode getTree() {
        if (analyzerTree == null) {
            analyzerTree = startNode;

            setStartNodeChildren();
            setRUONodeChildren();
            setLUONodeChildren();
            setVNodeChildren();
            setBONodeChildren();
            setOpeningBracketNodeChildren();
            setClosingBracketNodeChildren();
        }
        return analyzerTree;
    }

    private static void setStartNodeChildren() {
        startNode.addChild(luonode);
        startNode.addChild(openingBracketNode);
        startNode.addChild(vnode);
    }

    private static void setRUONodeChildren() {
        ruonode.addChild(closingBracketNode);
        ruonode.addChild(bonode);
        ruonode.addChild(endNode);
    }

    private static void setLUONodeChildren() {
        luonode.addChild(openingBracketNode);
        luonode.addChild(vnode);
    }

    private static void setVNodeChildren() {
        vnode.addChild(closingBracketNode);
        vnode.addChild(ruonode);
        vnode.addChild(bonode);
        vnode.addChild(endNode);
    }

    private static void setBONodeChildren() {
        bonode.addChild(vnode);
        bonode.addChild(openingBracketNode);
    }

    private static void setOpeningBracketNodeChildren() {
        openingBracketNode.addChild(openingBracketNode);
        openingBracketNode.addChild(luonode);
        openingBracketNode.addChild(vnode);
    }

    private static void setClosingBracketNodeChildren() {
        closingBracketNode.addChild(closingBracketNode);
        closingBracketNode.addChild(ruonode);
        closingBracketNode.addChild(bonode);
        closingBracketNode.addChild(endNode);
    }
}
