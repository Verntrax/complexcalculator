package calculator.analyzer;

import calculator.operation.tree.CalculationNode;

import java.util.List;

/**
 * Class for storing result of syntax analysis.
 */
public class AnalyzedStorage {
    /**
     * Name of a variable result of the expression
     * should be assigned to.
     */
    public final String assignmentName;
    /**
     * List of nodes parsed from the expression.
     */
    public final List<CalculationNode> nodes;

    AnalyzedStorage(String assignmentName, List<CalculationNode> nodes) {
        this.assignmentName = assignmentName;
        this.nodes = nodes;
    }

    /**
     * Checks if this storage has assignment name.
     * @return boolean indicating if this storage
     * has assignment name
     */
    public boolean hasAssignment() {
        return assignmentName != null;
    }
}
