package calculator.analyzer;

class VNode extends AnalyzerTreeNode {
    @Override
    void visit(SyntaxAnalyzer analyzer, String expression, int priority) {
        analyzer.accept(this, expression, priority);
    }
}
