package calculator.analyzer;

class BONode extends AnalyzerTreeNode {
    @Override
    void visit(SyntaxAnalyzer analyzer, String expression, int priority) {
        analyzer.accept(this, expression, priority);
    }
}
