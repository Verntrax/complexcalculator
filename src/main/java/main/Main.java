package main;

/**
 * Main project class.
 */
public class Main {
    /**
     * Main method starting the application.
     * @param args not used
     */
    public static void main(String[] args) {
        ConsoleCalculator calculator = new ConsoleCalculator();
        calculator.start();
    }
}
