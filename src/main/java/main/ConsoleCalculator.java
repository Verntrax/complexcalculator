package main;

import calculator.analyzer.SyntaxAnalyzer;
import calculator.exception.SyntaxError;
import calculator.calculator.ComplexCalculator;
import calculator.exception.VariableNotFoundException;
import calculator.operation.tree.*;
import complex.number.ComplexNumber;
import complex.number.exception.ComplexNumberException;
import main.exception.TerminationException;
import utils.Utils;

import java.text.MessageFormat;
import java.util.*;
import java.util.regex.Pattern;

class ConsoleCalculator {
    private static final String PROMPT = ">> ";
    private final Map<String, CalculatorAction> actions;
    private final List<Pattern> forbiddenAssignments;
    private final ComplexCalculator calculator;
    private final Scanner scanner;

    ConsoleCalculator() {
        ComplexCalculator.Builder builder = new ComplexCalculator.Builder();
        builder.addOperatorBuilder(NegateNode.Builder.getInstance());
        builder.addOperatorBuilder(ConjugateNode.Builder.getInstance());
        builder.addOperatorBuilder(PlusNode.Builder.getInstance());
        builder.addOperatorBuilder(MinusNode.Builder.getInstance());
        builder.addOperatorBuilder(MultiplyNode.Builder.getInstance());
        builder.addOperatorBuilder(DivideNode.Builder.getInstance());
        builder.addOperatorBuilder(PowerNode.Builder.getInstance());

        calculator = builder.build();
        scanner = new Scanner(System.in);

        actions = new HashMap<>();
        actions.put("print", this::print);
        actions.put("clear", this::clear);
        actions.put("exit", this::exit);

        forbiddenAssignments = new ArrayList<>(actions.size());

        for (String action : actions.keySet()) {
            Object[] params = {
                    action,
                    SyntaxAnalyzer.ASSIGNMENT
            };
            String regex = MessageFormat.format("{0}\\h*{1}", params);
            forbiddenAssignments.add(Pattern.compile(regex));
        }
    }

    void start() {
        while(true) {
            System.out.print(PROMPT);
            String expression = scanner.nextLine();
            if (isEmptyExpression(expression))
                continue;

            expression = Utils.removeLeadingSpaces(expression);

            try {
                perform(expression);
            } catch (SyntaxError e) {
                System.out.println("ERROR: Syntax error!");
            } catch (VariableNotFoundException e) {
                Object[] params = {e.getMessage()};
                String message = MessageFormat.format("ERROR: Variable \"{0}\" not found!", params);
                System.out.println(message);
            } catch (ComplexNumberException e) {
                System.out.println("ERROR: Calculation error in " + e.getMessage() + "!");
            } catch (TerminationException e) {
                scanner.close();
                break;
            } catch (Exception e){
                scanner.close();
            }
        }
    }
    private boolean isEmptyExpression(String expression) {
        return expression.replaceAll("\\h", "").equals("");
    }

    private void perform(String expression) throws VariableNotFoundException, SyntaxError, TerminationException {
        if (isForbiddenAssignment(expression))
            throw new SyntaxError();

        String[] args = expression.split("\\h+");
        CalculatorAction action = actions.get(args[0]);

        if (action != null)
            action.accept(args);
        else
            calculate(expression);
    }

    private boolean isForbiddenAssignment(String expression) {
        return forbiddenAssignments
                .stream()
                .anyMatch(x -> x.matcher(expression).lookingAt());
    }

    private void calculate(String expression) throws VariableNotFoundException, SyntaxError {
        System.out.println(calculator.calculate(expression));
    }

    private void print(String[] args) throws VariableNotFoundException {
        Map<String, ComplexNumber> vars = calculator.getVariables();

        if (args.length == 1)
            vars.entrySet()
            .stream()
            .sorted((x, y) -> String.CASE_INSENSITIVE_ORDER.compare(x.getKey(), y.getKey()))
            .forEach(x -> System.out.println(x.getKey() + " = " + x.getValue()));
        else {
            for (int i = 1; i < args.length; ++i) {
                if (vars.containsKey(args[i]))
                    System.out.println(args[i] + " = " + vars.get(args[i]));
                else
                    throw new VariableNotFoundException(args[i]);
            }
        }
    }

    private void clear(String[] args) {
        if (args.length == 1)
            calculator.clearVariables();
        else
            for (int i = 1; i < args.length; ++i)
                calculator.clearVariable(args[i]);
    }

    private void exit(String[] args) throws SyntaxError, TerminationException {
        if (args.length > 1)
            throw new SyntaxError();
        throw new TerminationException();
    }
}
