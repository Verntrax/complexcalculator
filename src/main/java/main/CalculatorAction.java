package main;

import calculator.exception.SyntaxError;
import calculator.exception.VariableNotFoundException;
import main.exception.TerminationException;

interface CalculatorAction {
    void accept(String[] args) throws VariableNotFoundException, SyntaxError, TerminationException;
}
