package main.exception;

/**
 * Exception thrown if calculator termination is
 * triggered.
 */
public class TerminationException extends Exception {}
