package complex.number;

import complex.number.exception.ComplexNumberException;
import utils.Pair;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * Complex number class storing real and imaginary
 * part of a number as doubles.
 */
public class ComplexNumber {
    private static final double PRECISION = 1e-12;
    private static final DecimalFormat numFormat;
    private static final DecimalFormat expFormat;

    /**
     * Real part of this complex number.
     */
    public final double re;
    /**
     * Imaginary part of this complex number.
     */
    public final double im;

    static {
        numFormat = (DecimalFormat) NumberFormat.getNumberInstance(Locale.ENGLISH);
        numFormat.applyPattern("#.#########");

        expFormat = (DecimalFormat) NumberFormat.getNumberInstance(Locale.ENGLISH);
        expFormat.applyPattern("#.##########E0");
    }

    /**
     * Creates complex number with given real and
     * imaginary part.
     * @param re real part of complex number
     * @param im imaginary part of complex number
     */
    public ComplexNumber(double re, double im) {
        this.re = re;
        this.im = im;
    }

    /**
     * Creates copy of a given complex number.
     * @param other complex number to be copied
     */
    public ComplexNumber(ComplexNumber other) {
        this.re = other.re;
        this.im = other.im;
    }

    /**
     * Negates this complex number.
     * @return negated number
     */
    public ComplexNumber negate() {
        double re = -this.re;
        double im = -this.im;

        return new ComplexNumber(re, im);
    }

    /**
     * Conjugates this complex number.
     * @return conjugated number
     */
    public ComplexNumber conjugate() {
        double re = this.re;
        double im = -this.im;

        return new ComplexNumber(re, im);
    }

    /**
     * Adds this number to the other one. If the
     * module of the result overflows double, this
     * method throws {@link ComplexNumberException}.
     * @param other number to be added to this number
     * @return result of addition
     */
    public ComplexNumber add(ComplexNumber other) {
        double re = this.re + other.re;
        double im = this.im + other.im;

        resultIsValid(re, im, "addition");
        return new ComplexNumber(re, im);
    }

    /**
     * Subtracts the other number from this one. If the
     * module of the result overflows double, this
     * method throws {@link ComplexNumberException}.
     * @param other number to be subtracted from this number
     * @return result of subtraction
     */
    public ComplexNumber subtract(ComplexNumber other) {
        double re = this.re - other.re;
        double im = this.im - other.im;

        resultIsValid(re, im, "subtraction");
        return new ComplexNumber(re, im);
    }

    /**
     * Multiplies this number by the other one. If the
     * module of the result overflows double, this
     * method throws {@link ComplexNumberException}.
     * @param other number to be multiplied by this number
     * @return result of subtraction
     */
    public ComplexNumber multiply(ComplexNumber other) {
        double re = this.re*other.re - this.im*other.im;
        double im = this.re*other.im + this.im*other.re;

        resultIsValid(re, im, "multiplication");
        return new ComplexNumber(re, im);
    }

    /**
     * Divides this number by the other one. If the
     * module of the result overflows double or division
     * by zero is identified, this method throws
     * {@link ComplexNumberException}. If this operation is
     * performed on two real numbers, the result is always real.
     * @param other number this number will be divided by
     * @return result of division
     */
    public ComplexNumber divide(ComplexNumber other) {
        double divMod = Math.hypot(other.re, other.im);

        double re = (this.re*other.re + this.im*other.im) / divMod / divMod;
        double im = (this.im*other.re - this.re*other.im) / divMod / divMod;

        resultIsValid(re, im, "division");
        return new ComplexNumber(re, im);
    }

    /**
     * Powers this number by the other one. If the
     * module of the result overflows double or any
     * mathematically undefined operation is performed
     * (see {@link Math#pow(double, double)}), this method
     * throws {@link ComplexNumberException}.
     * If this operation is performed on two real numbers
     * and the result of {@link Math#pow(double, double)}
     * for them is finite, the result is always real.
     * @param other number this number will be divided by
     * @return result of division
     */
    public ComplexNumber power(ComplexNumber other) {
        ComplexNumber result;

        if (im == 0 && other.im == 0 && (re >= 0 || Math.rint(other.re) == other.re))
            result = new ComplexNumber(Math.pow(re, other.re), 0);
        else {
            Pair<Double> converted = toExponential();
            double module = converted.first;
            double angle = converted.second;

            double newModule = Math.pow(module, other.re) * Math.exp(-other.im * angle);
            double newAngle = angle * other.re + other.im * Math.log(module);

            result = fromExponential(newModule, newAngle);
        }
        resultIsValid(result.re, result.im, "power");
        return result;
    }

    /**
     * Gives a {@link String} representing this number.
     * If real or imaginary part is equal to 0, it is not
     * included in the resulting {@link String} (except from
     * the case, when both of the are equal to 0 - then "0" is
     * the result).
     * @return {@link String} representing this number
     */
    @Override
    public String toString() {
        StringBuilder resultBuilder = new StringBuilder();

        if (re == 0 && im == 0)
            resultBuilder.append(format(0));
        else if (re == 0) {
            resultBuilder.append(format(im));
            resultBuilder.append('i');
        } else if (im == 0)
            resultBuilder.append(format(re));
        else {
            resultBuilder.append(format(re));
            if (im > 0)
                resultBuilder.append('+');
            resultBuilder.append(format(im))
                    .append('i');
        }
        return resultBuilder.toString();
    }

    /**
     * Checks if this number is equal to the other object.
     * Complex number are equal if the relative difference
     * of their modules and angles are lower than 1E-10.
     * @param other {@link Object} this number should be
     *                            compared to
     * @return true if the given {@link Object} is equal
     * to this number, false otherwise
     *
     */
    @Override
    public boolean equals(Object other) {
        if (other.getClass() != ComplexNumber.class)
            return false;

        Pair<Double> thisConverted = toExponential();
        Pair<Double> otherConverted = ((ComplexNumber) other).toExponential();

        return doubleIsEqual(thisConverted.first, otherConverted.first)
                && doubleIsEqual(thisConverted.second, otherConverted.second);
    }

    private static String format(double num) {
        double module = Math.abs(num);

        if (num == 0 || (1e-3 <= module && module < 1e7))
            return numFormat.format(num);
        return expFormat.format(num);
    }

    private static void resultIsValid(double re, double im, String operationName) {
        if (!Double.isFinite(Math.hypot(re, im)))
            throw new ComplexNumberException(operationName);
    }

    private static boolean doubleIsEqual(double a, double b) {
        if (a == 0 && b == 0)
            return true;
        double result = Math.abs(a - b)/Math.max(Math.abs(a), Math.abs(b));
        return result < PRECISION;
    }

    private static ComplexNumber fromExponential(double module, double angle) {
        double re = module * Math.cos(angle);
        double im = module * Math.sin(angle);

        return new ComplexNumber(re, im);
    }

    private Pair<Double> toExponential() {
        double module = Math.hypot(re, im);
        double angle = Math.atan2(im, re);

        if (angle < 0)
            angle += 2*Math.PI;

        return new Pair<>(module, angle);
    }
}
