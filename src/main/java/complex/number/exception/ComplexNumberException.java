package complex.number.exception;

/**
 * Exception thrown every time any calculation operation
 * on ComplexNumber object fails.
 */
public class ComplexNumberException extends RuntimeException {
    /**
     * Creates exception holding the given message.
     * @param message message to be held by this exception
     */
    public ComplexNumberException(String message) {
        super(message);
    }
}
